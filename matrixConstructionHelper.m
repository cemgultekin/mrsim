function [A,errs]=matrixConstructionHelper(k,control,m0s,T1,T2f,R,T2s,TR,...
    type1stepnum,degree,devs,errmes)
errs=0;
if k==1
    th2=control(k,1);
    if devs==0
        [A,errs]=MRsim.intervalType2(th2,0,m0s,T1,T2f,R,T2s,TR,errmes);
        return; 
    elseif devs==1
        [M0,errs,M0th2,M0TRF]=MRsim.intervalType2(th2,0,m0s,T1,T2f,R,T2s,TR,errmes);
        A=[M0;M0th2.*0;M0th2;M0TRF];
        return;
    else
        [M0,errs,M0th2,M0TRF,M0th2th2,M0th2TRF,M0TRFTRF]=MRsim.intervalType2(th2,0,m0s,T1,T2f,R,T2s,TR,errmes);
        %th1, th2, TRF, th1th1, th1th2, th1TRF, th2th2, th2TRF, TRFTRF
        Z=M0th2.*0;
        A=[M0;Z;M0th2;M0TRF;Z;Z;Z;M0th2th2;M0th2TRF;M0TRFTRF];
        return;
    end
end
if k==size(control,1)+1
    th1=control(k-1,1);
    if devs==0
        [A,errs]=MRsim.intervalType2(th1,0,m0s,T1,T2f,R,T2s,TR,errmes);
        return; 
    elseif devs==1
        [M0,errs,M0th1,M0TRF]=MRsim.intervalType2(th1,0,m0s,T1,T2f,R,T2s,TR,errmes);
        A=[M0;M0th1;M0th1.*0;M0TRF];
        return;
    else
        [M0,errs,M0th1,M0TRF,M0th1th1,M0th1TRF,M0TRFTRF]=MRsim.intervalType2(th1,0,m0s,T1,T2f,R,T2s,TR,errmes);
        %th1, th2, TRF, th1th1, th1th2, th1TRF, th2th2, th2TRF, TRFTRF
        Z=M0th1.*0;
        A=[M0;M0th1;Z;M0TRF;M0th1th1;Z;M0th1TRF;Z;Z;M0TRFTRF];
        return;
    end
end
th1=control(k-1,1);
th2=control(k,1);
TRF=control(k-1,2);



if devs==0
    [M0,err]=MRsim.intervalType2(th1,TRF,m0s,T1,T2f,R,T2s,TR,errmes);
    errs=errs+err;
    [M1,err]=MRsim.multiStepType1(-th1,th2,type1stepnum,degree,TRF,m0s,T1,T2f,R,T2s,TR,errmes);
    errs=errs+err;
    [M2,err]=MRsim.intervalType2(th2,TRF,m0s,T1,T2f,R,T2s,TR,errmes);
    errs=errs+err;
    A=M2*M1*M0;
    return;
end
if devs==1
    [M0,err,M0th1,M0TRF]=MRsim.intervalType2(th1,TRF,m0s,T1,T2f,R,T2s,TR,errmes);
    errs=errs+err;
    [M1,err,M1th1,M1th2,M1TRF]=MRsim.multiStepType1(-th1,th2,type1stepnum,degree,TRF,m0s,T1,T2f,R,T2s,TR,errmes);
    errs=errs+err;
    [M2,err,M2th2,M2TRF]=MRsim.intervalType2(th2,TRF,m0s,T1,T2f,R,T2s,TR,errmes);
    errs=errs+err;

    M=M2*M1*M0;
    Mth1=-M2*M1th1*M0+M2*M1*M0th1;
    Mth2=M2th2*M1*M0+M2*M1th2*M0;
    MTRF=M2TRF*M1*M0+M2*M1TRF*M0+M2*M1*M0TRF;
    A=[M;Mth1;Mth2;MTRF];
    return;
end

[M0,err,M0th1,M0TRF,...
    M0th1th1,M0th1TRF,M0TRFTRF]=MRsim.intervalType2(th1,TRF,m0s,T1,T2f,R,T2s,TR,errmes);
errs=errs+err;
[M1,err,M1th1,M1th2,M1TRF,...
    M1th1th1,M1th1th2,M1th1TRF,M1th2th2,M1th2TRF,M1TRFTRF]=MRsim.multiStepType1(-th1,th2,type1stepnum,degree,TRF,m0s,T1,T2f,R,T2s,TR,errmes);
errs=errs+err;
[M2,errs,M2th2,M2TRF,...
    M2th2th2,M2th2TRF,M2TRFTRF]=MRsim.intervalType2(th2,TRF,m0s,T1,T2f,R,T2s,TR,errmes);
errs=errs+err;

M=M2*M1*M0;
Mth1=-M2*M1th1*M0+M2*M1*M0th1;
Mth2=M2th2*M1*M0+M2*M1th2*M0;
MTRF=M2TRF*M1*M0+M2*M1TRF*M0+M2*M1*M0TRF;


Mth1th1=M2*M1*M0th1th1+M2*M1th1th1*M0+...
    -2*M2*M1th1*M0th1;
Mth1th2=-M2*M1th1th2*M0...
    -M2th2*M1th1*M0+M2th2*M1*M0th1+...
    M2*M1th2*M0th1;
Mth1TRF=-M2TRF*M1th1*M0+M2TRF*M1*M0th1+...
    -M2*M1th1TRF*M0+M2*M1TRF*M0th1+...
    -M2*M1th1*M0TRF+M2*M1*M0th1TRF;

Mth2th2=M2*M1th2th2*M0+M2th2th2*M1*M0...
    +2*M2th2*M1th2*M0;

Mth2TRF=M2th2TRF*M1*M0+M2*M1th2TRF*M0+...
    M2TRF*M1th2*M0+...
    M2th2*M1TRF*M0+ ...
    M2*M1th2*M0TRF+ M2th2*M1*M0TRF;


MTRFTRF=M2TRFTRF*M1*M0+M2*M1TRFTRF*M0+M2*M1*M0TRFTRF+...
    2*(M2TRF*M1TRF*M0+ M2TRF*M1*M0TRF+ M2*M1TRF*M0TRF);

A=[M;Mth1;Mth2;MTRF; Mth1th1; Mth1th2; Mth1TRF; Mth2th2; Mth2TRF; MTRFTRF];
end