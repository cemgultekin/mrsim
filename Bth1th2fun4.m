function Bth1th2 = Bth1th2fun4(th1,th2)
%BTH1TH2FUN4
%    BTH1TH2 = BTH1TH2FUN4(TH1,TH2)

%    This function was generated by the Symbolic Math Toolbox version 8.2.
%    28-May-2020 20:18:17

t2 = cos(th1);
t3 = cos(th2);
t4 = sin(th2);
t5 = sin(th1);
t6 = th1.*2.0;
t7 = cos(t6);
t8 = th2.*2.0;
t9 = cos(t8);
t10 = t3.^2;
t11 = t4.^2;
t12 = sin(t6);
t13 = sin(t8);
t14 = -t4+t5;
t15 = t2.^2;
t16 = t5.^2;
t17 = t5.*t15;
t18 = t3.*t5;
t19 = t5.*t10;
t20 = t4.*t16;
t21 = t2.*t3.*t5;
t22 = th1-th2;
t23 = th1.^2;
t24 = th2.^2;
t25 = th2.*3.0;
t26 = sin(t25);
t27 = t2.*t3;
t28 = t2.*t11;
t29 = (t3.*t15)./4.0;
t30 = t4.*t5;
t31 = t3.*t4.*t5;
t32 = t10./4.0;
t33 = t10.^2;
t34 = t7./4.0;
t35 = (t3.*t10)./6.0;
t36 = t13.^2;
t37 = (t7.*t9)./4.0;
t38 = t2.*t4.*t5;
t39 = t9.^2;
t40 = t7.^2;
t41 = t12.^2;
t42 = (t3.*t16)./2.0;
t43 = t10./2.0;
t44 = t2.*t4.*th1;
t45 = t2.*t3.*t4.*th1;
t46 = t2.*t3.*(2.0./9.0);
t48 = th1./2.0;
t47 = sin(t48);
t49 = (t2.*t11)./2.0;
t50 = t2.*t3.*t5.*th2;
t51 = t11.*(3.0./4.0);
t52 = (t10.*t15)./4.0;
t53 = (t2.*t15)./6.0;
t54 = (t2.*t10.*t15)./6.0;
t55 = (t2.*t11.*t16)./3.0;
t56 = t2.*t4.*t5.*t10.*2.0;
t57 = t15.^2;
t58 = t2.*t3.*t4.*t5;
t59 = t2./4.0;
t60 = (t2.*t10)./4.0;
t61 = t2.*t4.*t5.*t10;
t62 = t10./8.0;
t63 = (t2.*t11)./4.0;
t64 = t2./8.0;
t65 = (t2.*t33)./4.0;
t66 = t15./4.0;
t67 = t11./4.0;
t68 = (t11.*t15)./4.0;
t69 = (t11.*t16)./4.0;
t70 = t4.*t5.*t15;
t71 = (t2.*t33)./8.0;
t72 = t3.*t4.*t5.*t15;
t73 = t2.*t3.*(2.0./1.5e1);
t74 = (t2.*t33)./6.0;
t75 = (t3.*t16)./4.0;
t76 = t3.*t16;
t77 = (t10.*t16)./4.0;
t78 = t4.*t16.*th2;
t79 = t4.*t15.*th1;
t80 = t3./8.0;
t81 = t2./2.0;
t82 = th1.*3.0;
t83 = sin(t82);
t84 = t15./2.0;
t85 = t2.*t3.*t15.*(2.0./9.0);
t86 = (t10.*t15)./2.0;
t87 = (t11.*t16)./2.0;
t88 = t3.*t5.*th2;
t89 = t2.*t10.*t15.*(2.0./9.0);
t90 = t5.*t11.*th1;
t91 = t5.*t10.*th2;
t92 = (t3.*t57)./4.0;
t93 = t15./8.0;
t94 = t11.*t15.*(3.0./4.0);
t95 = t10.*t16.*(3.0./4.0);
t96 = t4.*t5.*t15.*2.0;
t97 = t4.*t5.*t10.*2.0;
t99 = t3.*t11;
t98 = t3-t99;
t100 = t3.*t4.*t5.*t15.*2.0;
t101 = t16./4.0;
t102 = t4.*t5.*t10;
t103 = t4.*t5.*t10.*t15.*2.0;
t104 = t3.*t4.*t5.*t10.*2.0;
t105 = (t3.*t57)./8.0;
t106 = (t3.*t10.*t15)./6.0;
t107 = (t3.*t11.*t16)./3.0;
t108 = (t9.*t40)./1.6e1;
t109 = (t10.*t15)./8.0;
t110 = (t11.*t16)./8.0;
t111 = (t2.*t11)./1.5e1;
t112 = t2.*t4.*t5.*t15.*2.0;
t113 = (t3.*t57)./6.0;
t114 = (t3.*t16)./1.5e1;
t115 = t12-t13;
Bth1th2 = [0.0;0.0;0.0;-1.0;-t3;-t9;-t2;-t2.*t3;t28-t2.*t10;-t7;t76-t3.*t15;-t7.*t9;t22;t3.*th1-t3.*th2;t9.*th1-t9.*th2;t14;t18-t3.*t4;t4+t19-t4.*t10.*2.0-t5.*t11;t12./2.0-t13./2.0;t21-t4.*t10;(t9.*t12)./2.0-(t9.*t13)./2.0;t2.*th1-t2.*th2;t2.*t3.*th1-t2.*t3.*th2;t2.*t10.*th1-t2.*t10.*th2-t2.*t11.*th1+t2.*t11.*th2;-t2.*t4+t2.*t5;t2.*t3.*t14;t2.*t4-t2.*t4.*t10.*2.0+t2.*t5.*t10-t2.*t5.*t11;t17-t2.*t3.*t4;-t2.*t4.*t10+t3.*t5.*t15;t2.*t3.*t4+t5.*t10.*t15-t5.*t11.*t15-t2.*t3.*t4.*t10.*2.0;t7.*th1-t7.*th2;t3.*t15.*th1-t3.*t15.*th2-t3.*t16.*th1+t3.*t16.*th2;t7.*t9.*th1-t7.*t9.*th2;t17+t20-t4.*t15-t5.*t16;-t18-t3.*t4.*t15+t3.*t4.*t16+t3.*t5.*t15.*2.0;-t19-t20+t5.*t11+t4.*t15-t4.*t10.*t15.*2.0+t4.*t10.*t16.*2.0+t5.*t10.*t15.*2.0-t5.*t11.*t15.*2.0;(t7.*t12)./2.0-(t7.*t13)./2.0;-t21-t4.*t10.*t15+t4.*t10.*t16+t2.*t3.*t5.*t15.*2.0;(t7.*t9.*t115)./2.0;t22.^2.*(-1.0./2.0);t3.*t23.*(-1.0./2.0)-(t3.*t24)./2.0+t3.*th1.*th2;t9.*t23.*(-1.0./2.0)-(t9.*t24)./2.0+t9.*th1.*th2;t2-t3+t4.*th1-t4.*th2;t10.*(-1.0./4.0)+t27+t51+t3.*t4.*th1-t3.*t4.*th2-3.0./4.0;t3.*(-1.0./2.0)-cos(t25)./2.0+t2.*t9-(t4.*th1)./2.0+(t4.*th2)./2.0+(t26.*th1)./2.0-(t26.*th2)./2.0;t9.*(-1.0./4.0)+t34+(t13.*th1)./2.0-(t13.*th2)./2.0;t3.*(-1.1e1./3.6e1)+t29+(t3.*t10)./1.8e1+t3.*t11.*(5.0./9.0)-(t3.*t16)./4.0+t4.*t10.*th1-t4.*t10.*th2;t36.*(-1.0./8.0)+t37+t9.*(t11.*2.0-1.0).*(3.0./1.6e1)+t3.*t4.*t13.*(3.0./8.0)+(t9.*t13.*th1)./2.0-(t9.*t13.*th2)./2.0-1.0./1.6e1;-t2+t3-t5.*th1+t5.*th2;t11.*(-1.0./2.0)-t27+t43+t88-t3.*t5.*th1+1.0./2.0;t3./3.0+t28+t90+t91-t2.*t10+t3.*t10.*(2.0./3.0)-t3.*t11.*(4.0./3.0)-t5.*t10.*th1-t5.*t11.*th2;t11.*(-1.0./4.0)-t16./4.0+t30+t32+t66-1.0./2.0;t3.*(-5.0./1.2e1)+t29+t31+t35-(t3.*t11)./3.0-(t3.*t16)./4.0;t10.*(-5.0./8.0)+t11.*(5.0./8.0)-t30+t33./4.0+t52+t69+t97-t10.*t11.*(3.0./4.0)-(t10.*t16)./4.0-(t11.*t15)./4.0+1.0./8.0;t2.*(2.0./9.0)-t3.*(5.0./9.0)+t31+t3.*t10.*(2.0./9.0)-t3.*t11.*(4.0./9.0)+(t2.*t15)./9.0-t2.*t16.*(2.0./9.0);-t32+t33./6.0+t46+t67+t102-(t10.*t11)./2.0+(t2.*t3.*t15)./9.0-t2.*t3.*t16.*(2.0./9.0)-1.0./4.0;t3.*(-1.0./1.5e1)-t31+t104+t2.*t10.*(2.0./9.0)-t2.*t11.*(2.0./9.0)-t3.*t10.*(8.0./1.5e1)+t3.*t11.*(1.6e1./1.5e1)+t3.*t33.*(4.0./1.5e1)-t3.*t10.*t11.*(1.6e1./1.5e1)+(t2.*t10.*t15)./9.0-t2.*t10.*t16.*(2.0./9.0)-(t2.*t11.*t15)./9.0+t2.*t11.*t16.*(2.0./9.0);t9./4.0-t34-(t12.*th1)./2.0+(t12.*th2)./2.0;t3.*(-1.0./6.0)+t35+t42+t50-(t3.*t11)./3.0-t2.*t3.*t5.*th1;t36.*(-1.0./8.0)-t37+t39./8.0-(t9.*t12.*th1)./2.0+(t9.*t12.*th2)./2.0+1.0./8.0;t2.*(-5.0./9.0)+t3.*(2.0./9.0)+t38+(t3.*t10)./9.0-t3.*t11.*(2.0./9.0)+t2.*t15.*(2.0./9.0)-t2.*t16.*(4.0./9.0);t11.*(-1.0./8.0)+t33./1.2e1+t58+t62+t85-t2.*t3.*(5.0./9.0)-(t10.*t11)./4.0-t2.*t3.*t16.*(4.0./9.0)+1.0./8.0;t3.*(2.0./1.5e1)-t38+t56+t89-t2.*t10.*(5.0./9.0)+t2.*t11.*(5.0./9.0)+(t3.*t10)./1.5e1-t3.*t11.*(2.0./1.5e1)+t3.*t33.*(2.0./1.5e1)-t3.*t10.*t11.*(8.0./1.5e1)-t2.*t10.*t16.*(4.0./9.0)-t2.*t11.*t15.*(2.0./9.0)+t2.*t11.*t16.*(4.0./9.0);t36.*(-1.0./1.6e1)+t39./1.6e1+t40./1.6e1-t41./1.6e1+(t12.*t13)./4.0-1.0./8.0;t3.*(-3.1e1./2.4e2)+t61+t105-(t3.*t10)./3.0e1+(t3.*t11)./1.5e1-(t3.*t15)./1.6e1+(t3.*t16)./1.6e1+(t3.*t33)./1.0e1-t3.*t10.*t11.*(2.0./5.0)-t3.*t15.*t16.*(3.0./8.0);t9.*(-5.0./4.8e1)+t108-(t9.*t36)./1.2e1+(t9.*t39)./2.4e1-(t9.*t41)./1.6e1+(t9.*t12.*t13)./4.0;t2.*t23.*(-1.0./2.0)-(t2.*t24)./2.0+t2.*th1.*th2;t2.*t3.*t23.*(-1.0./2.0)-(t2.*t3.*t24)./2.0+t2.*t3.*th1.*th2;t59-(t2.*t10)./4.0-(t2.*t11)./4.0-(t2.*t10.*t23)./2.0-(t2.*t10.*t24)./2.0+(t2.*t11.*t23)./2.0+(t2.*t11.*t24)./2.0+t2.*t10.*th1.*th2-t2.*t11.*th1.*th2;t16.*(-1.0./2.0)-t27+t44+t84-t2.*t4.*th2+1.0./2.0;t2.*(-3.0./4.0)+t3./2.0-t42+t45-(t2.*t10)./4.0+t2.*t11.*(3.0./4.0)+(t3.*t15)./2.0-t2.*t3.*t4.*th2;t11.*(-1.0./2.0)+t43-t44+t86+t87-t2.*t3.*(7.0./9.0)-(t10.*t16)./2.0-(t11.*t15)./2.0-t2.*t3.*t10.*(2.0./9.0)+t2.*t3.*t11.*(1.6e1./9.0)+t2.*t4.*th2+t2.*t4.*t10.*th1.*2.0-t2.*t4.*t10.*th2.*2.0;t2.*(-1.0./6.0)+t45+t49+t53-(t2.*t16)./3.0-t2.*t3.*t4.*th2;-t46+(t2.*t3.*t10)./1.8e1+t2.*t3.*t11.*(5.0./9.0)+(t2.*t3.*t15)./6.0-(t2.*t3.*t16)./3.0+t2.*t4.*t10.*th1-t2.*t4.*t10.*th2;t2.*(-1.0./1.6e1)-t45+t54+t55+t71-t2.*t10.*(1.1e1./4.8e1)-t2.*t11.*(1.3e1./4.8e1)+t2.*t10.*t11.*(9.0./8.0)-(t2.*t10.*t16)./3.0-(t2.*t11.*t15)./6.0+t2.*t3.*t4.*th2+t2.*t3.*t4.*t10.*th1.*2.0-t2.*t3.*t4.*t10.*th2.*2.0;t16.*(-1.0./2.0)+t27+t2.*(t47.^2.*2.0-1.0).*(3.0./4.0)+t5.*t47.*cos(t48).*(3.0./2.0)-t2.*t5.*th1+t2.*t5.*th2-1.0./4.0;t3.*(-3.0./4.0)-t29-t49+t50+t81+(t2.*t10)./2.0+t3.*t16.*(3.0./4.0)-t2.*t3.*t5.*th1;t10.*(-3.0./4.0)+t51-t52+t68+t95+(t2.*t3)./3.0-t11.*t16.*(3.0./4.0)+t2.*t3.*t10.*(2.0./3.0)-t2.*t3.*t11.*(4.0./3.0)-t2.*t5.*t10.*th1+t2.*t5.*t10.*th2+t2.*t5.*t11.*th1-t2.*t5.*t11.*th2;t2.*(-5.0./1.2e1)+t38+t53+t60-(t2.*t11)./4.0-(t2.*t16)./3.0;t2.*t3.*t14.^2.*(-1.0./2.0);-t38+t54+t55+t56+t64+t65-t2.*t10.*(1.3e1./2.4e1)+t2.*t11.*(1.3e1./2.4e1)-t2.*t10.*t11.*(3.0./4.0)-(t2.*t10.*t16)./3.0-(t2.*t11.*t15)./6.0;t16.*(-1.0./8.0)+t57./1.2e1+t58+t93-t2.*t3.*(5.0./9.0)-(t15.*t16)./4.0+t2.*t3.*t10.*(2.0./9.0)-t2.*t3.*t11.*(4.0./9.0)+1.0./8.0;-t59-t60+t61+t63+t74+t80+(t3.*t15)./8.0-(t3.*t16)./8.0+(t3.*t57)./1.2e1-(t2.*t10.*t11)./2.0-(t3.*t15.*t16)./4.0;t11.*(-1.0./8.0)-t58+t62+t109+t110-(t2.*t3)./1.5e1-(t10.*t16)./8.0-(t11.*t15)./8.0+(t10.*t57)./1.2e1-(t11.*t57)./1.2e1-t2.*t3.*t10.*(8.0./1.5e1)+t2.*t3.*t11.*(1.6e1./1.5e1)+t2.*t3.*t33.*(4.0./1.5e1)-(t10.*t15.*t16)./4.0+(t11.*t15.*t16)./4.0-t2.*t3.*t10.*t11.*(1.6e1./1.5e1)+t2.*t3.*t4.*t5.*t10.*2.0;t2.*(-1.1e1./3.6e1)+t60-t63+(t2.*t15)./1.8e1+t2.*t16.*(5.0./9.0)-t5.*t15.*th1+t5.*t15.*th2;-t46+(t2.*t3.*t10)./6.0-(t2.*t3.*t11)./3.0+(t2.*t3.*t15)./1.8e1+t2.*t3.*t16.*(5.0./9.0)-t3.*t5.*t15.*th1+t3.*t5.*t15.*th2;t64+t65-t2.*t10.*(3.1e1./7.2e1)+t2.*t11.*(3.1e1./7.2e1)-t2.*t10.*t11.*(3.0./4.0)+(t2.*t10.*t15)./1.8e1+t2.*t10.*t16.*(5.0./9.0)-(t2.*t11.*t15)./1.8e1-t2.*t11.*t16.*(5.0./9.0)-t5.*t10.*t15.*th1+t5.*t10.*t15.*th2+t5.*t11.*t15.*th1-t5.*t11.*t15.*th2;t57./6.0-t66+t70+t101-(t15.*t16)./2.0+(t2.*t98)./3.0-1.0./4.0;t3.*(-1.0./4.0)-t29+t64+t72+t75+t113+(t2.*t10)./8.0-(t2.*t11)./8.0+(t2.*t33)./1.2e1-(t2.*t10.*t11)./4.0-(t3.*t15.*t16)./2.0;-t32-t52+t67+t68-t69-t70+t73+t77+t103+(t10.*t57)./6.0-(t11.*t57)./6.0+(t2.*t3.*t10)./1.5e1-t2.*t3.*t11.*(2.0./1.5e1)+t2.*t3.*t33.*(2.0./1.5e1)-(t10.*t15.*t16)./2.0+(t11.*t15.*t16)./2.0-t2.*t3.*t10.*t11.*(8.0./1.5e1);t2.*(-3.1e1./2.4e2)+t71+t72-(t2.*t10)./1.6e1+(t2.*t11)./1.6e1-(t2.*t15)./3.0e1+(t2.*t16)./1.5e1+(t2.*t57)./1.0e1-t2.*t10.*t11.*(3.0./8.0)-t2.*t15.*t16.*(2.0./5.0);-t73-(t2.*t3.*t10)./3.0e1+(t2.*t3.*t11)./1.5e1-(t2.*t3.*t15)./3.0e1+(t2.*t3.*t16)./1.5e1+(t2.*t3.*t33)./1.0e1+(t2.*t3.*t57)./1.0e1-t2.*t3.*t10.*t11.*(2.0./5.0)+t4.*t5.*t10.*t15-t2.*t3.*t15.*t16.*(2.0./5.0);-t72-t74+t111-(t2.*t10)./1.5e1+(t2.*t10.*t11)./2.0-(t2.*t10.*t15)./3.0e1+(t2.*t10.*t16)./1.5e1+(t2.*t11.*t15)./3.0e1-(t2.*t11.*t16)./1.5e1+(t2.*t10.*t33)./6.0-t2.*t11.*t33.*(5.0./6.0)+(t2.*t10.*t57)./1.0e1-(t2.*t11.*t57)./1.0e1-t2.*t10.*t15.*t16.*(2.0./5.0)+t2.*t11.*t15.*t16.*(2.0./5.0)+t3.*t4.*t5.*t10.*t15.*2.0;t7.*t23.*(-1.0./2.0)-(t7.*t24)./2.0+t7.*th1.*th2;t3./4.0-t29-t75-(t3.*t15.*t23)./2.0-(t3.*t15.*t24)./2.0+(t3.*t16.*t23)./2.0+(t3.*t16.*t24)./2.0+t3.*t15.*th1.*th2-t3.*t16.*th1.*th2;t7.*t9.*t23.*(-1.0./2.0)-(t7.*t9.*t24)./2.0+t7.*t9.*th1.*th2;t2./3.0+t76+t78+t79+t2.*t15.*(2.0./3.0)-t2.*t16.*(4.0./3.0)-t3.*t15-t4.*t15.*th2-t4.*t16.*th1;t15.*(-3.0./4.0)+t16.*(3.0./4.0)-t52+t77+t94-t11.*t16.*(3.0./4.0)+t2.*t3.*t15-t2.*t3.*t16+t3.*t4.*t15.*th1-t3.*t4.*t15.*th2-t3.*t4.*t16.*th1+t3.*t4.*t16.*th2;-t78-t79+(t2.*t10)./3.0-(t2.*t11)./3.0-t3.*t15.*(7.0./9.0)+t3.*t16.*(7.0./9.0)+t2.*t10.*t15.*(2.0./3.0)-t2.*t10.*t16.*(4.0./3.0)-t2.*t11.*t15.*(2.0./3.0)-t3.*t10.*t15.*(2.0./9.0)+t2.*t11.*t16.*(4.0./3.0)+t3.*t10.*t16.*(2.0./9.0)+t3.*t11.*t15.*(1.6e1./9.0)-t3.*t11.*t16.*(1.6e1./9.0)+t4.*t15.*th2+t4.*t16.*th1+t4.*t10.*t15.*th1.*2.0-t4.*t10.*t15.*th2.*2.0-t4.*t10.*t16.*th1.*2.0+t4.*t10.*t16.*th2.*2.0;t7.*(3.0./8.0)-t58-(t7.*t9)./2.0-(t7.*t11)./2.0-(t7.*t16)./4.0-(t2.*t5.*t12)./4.0+(t3.*t4.*t12)./2.0+t3.*t4.*t7.*th1-t3.*t4.*t7.*th2+1.0./8.0;t80+t92-t3.*t15.*(3.1e1./7.2e1)+t3.*t16.*(3.1e1./7.2e1)+(t3.*t10.*t15)./1.8e1-(t3.*t10.*t16)./1.8e1+t3.*t11.*t15.*(5.0./9.0)-t3.*t11.*t16.*(5.0./9.0)-t3.*t15.*t16.*(3.0./4.0)+t4.*t10.*t15.*th1-t4.*t10.*t15.*th2-t4.*t10.*t16.*th1+t4.*t10.*t16.*th2;t7.*(-3.0./1.6e1)+t9./8.0+t7.*t36.*(3.0./1.6e1)-(t7.*t39)./1.6e1+(t9.*t40)./8.0-(t9.*t41)./8.0+(t7.*t9.*t13.*th1)./2.0-(t7.*t9.*t13.*th2)./2.0;-t81-cos(t82)./2.0+t3.*t7+(t5.*th1)./2.0-(t5.*th2)./2.0-(t83.*th1)./2.0+(t83.*th2)./2.0;t16.*(-1.0./2.0)+t84-t85+t86+t87-t88-t2.*t3.*(7.0./9.0)-(t10.*t16)./2.0-(t11.*t15)./2.0+t2.*t3.*t16.*(1.6e1./9.0)+t3.*t5.*th1-t3.*t5.*t15.*th1.*2.0+t3.*t5.*t15.*th2.*2.0;-t89-t90-t91-t2.*t10.*(7.0./9.0)+t2.*t11.*(7.0./9.0)+(t3.*t15)./3.0-(t3.*t16)./3.0+t2.*t10.*t16.*(1.6e1./9.0)+t2.*t11.*t15.*(2.0./9.0)+t3.*t10.*t15.*(2.0./3.0)-t2.*t11.*t16.*(1.6e1./9.0)-t3.*t10.*t16.*(2.0./3.0)-t3.*t11.*t15.*(4.0./3.0)+t3.*t11.*t16.*(4.0./3.0)+t5.*t10.*th1+t5.*t11.*th2-t5.*t10.*t15.*th1.*2.0+t5.*t10.*t15.*th2.*2.0+t5.*t11.*t15.*th1.*2.0-t5.*t11.*t15.*th2.*2.0;t15.*(-5.0./8.0)+t16.*(5.0./8.0)-t30+t52+t57./4.0-t68+t69-t77+t96-t15.*t16.*(3.0./4.0)+1.0./8.0;-t31+t80+t92+t100+t106+t107-t3.*t15.*(1.3e1./2.4e1)+t3.*t16.*(1.3e1./2.4e1)-(t3.*t10.*t16)./6.0-(t3.*t11.*t15)./3.0-t3.*t15.*t16.*(3.0./4.0);t11.*(-1.0./8.0)-t16./8.0+t30+t62+t93+t94+t95-t96-t97-t10.*t15.*(3.0./4.0)-t11.*t16.*(3.0./4.0)+(t15.*t33)./4.0-(t16.*t33)./4.0+(t10.*t57)./4.0-(t11.*t57)./4.0-t10.*t11.*t15.*(3.0./4.0)+t10.*t11.*t16.*(3.0./4.0)-t10.*t15.*t16.*(3.0./4.0)+t11.*t15.*t16.*(3.0./4.0)+t4.*t5.*t10.*t15.*4.0;t2.*(2.0./1.5e1)-t31+t76+t100+(t2.*t15)./1.5e1-t2.*t16.*(2.0./1.5e1)-t3.*t15+t2.*t57.*(2.0./1.5e1)+t15.*t98.*(2.0./3.0)-t16.*t98.*(2.0./3.0)-t2.*t15.*t16.*(8.0./1.5e1);-t52-t66+t68-t69+t73+t77+t101-t102+t103+(t15.*t33)./6.0-(t16.*t33)./6.0+(t2.*t3.*t15)./1.5e1-t2.*t3.*t16.*(2.0./1.5e1)-(t10.*t11.*t15)./2.0+(t10.*t11.*t16)./2.0+t2.*t3.*t57.*(2.0./1.5e1)-t2.*t3.*t15.*t16.*(8.0./1.5e1);t31-t100-t104+t114+t2.*t10.*(2.0./1.5e1)-t2.*t11.*(2.0./1.5e1)-(t3.*t15)./1.5e1+(t2.*t10.*t15)./1.5e1-t2.*t10.*t16.*(2.0./1.5e1)-(t2.*t11.*t15)./1.5e1-t3.*t10.*t15.*(8.0./1.5e1)+t2.*t11.*t16.*(2.0./1.5e1)+t3.*t10.*t16.*(8.0./1.5e1)+t3.*t11.*t15.*(1.6e1./1.5e1)-t3.*t11.*t16.*(1.6e1./1.5e1)+t3.*t15.*t33.*(4.0./1.5e1)-t3.*t16.*t33.*(4.0./1.5e1)+t2.*t10.*t57.*(2.0./1.5e1)-t2.*t11.*t57.*(2.0./1.5e1)-t3.*t10.*t11.*t15.*(1.6e1./1.5e1)+t3.*t10.*t11.*t16.*(1.6e1./1.5e1)-t2.*t10.*t15.*t16.*(8.0./1.5e1)+t2.*t11.*t15.*t16.*(8.0./1.5e1)+t3.*t4.*t5.*t10.*t15.*4.0;t37-t41./8.0+t7.*(t16.*2.0-1.0).*(3.0./1.6e1)+t2.*t5.*t12.*(3.0./8.0)-(t7.*t12.*th1)./2.0+(t7.*t12.*th2)./2.0-1.0./1.6e1;t3.*(-1.0./1.6e1)-t50+t105+t106+t107-t3.*t15.*(1.1e1./4.8e1)-t3.*t16.*(1.3e1./4.8e1)-(t3.*t10.*t16)./6.0-(t3.*t11.*t15)./3.0+t3.*t15.*t16.*(9.0./8.0)+t2.*t3.*t5.*th1-t2.*t3.*t5.*t15.*th1.*2.0+t2.*t3.*t5.*t15.*th2.*2.0;t7./8.0-t9.*(3.0./1.6e1)-t108-(t7.*t36)./8.0+(t7.*t39)./8.0+t9.*t41.*(3.0./1.6e1)-(t7.*t9.*t12.*th1)./2.0+(t7.*t9.*t12.*th2)./2.0;t2.*(-1.0./1.5e1)-t38+t112-t2.*t15.*(8.0./1.5e1)+t2.*t16.*(1.6e1./1.5e1)+t2.*t57.*(4.0./1.5e1)+(t15.*t98)./3.0-(t16.*t98)./3.0-t2.*t15.*t16.*(1.6e1./1.5e1);t16.*(-1.0./8.0)-t58+t93+t109+t110-(t2.*t3)./1.5e1-(t10.*t16)./8.0-(t11.*t15)./8.0+(t15.*t33)./1.2e1-(t16.*t33)./1.2e1-t2.*t3.*t15.*(8.0./1.5e1)+t2.*t3.*t16.*(1.6e1./1.5e1)-(t10.*t11.*t15)./4.0+(t10.*t11.*t16)./4.0+t2.*t3.*t57.*(4.0./1.5e1)-t2.*t3.*t15.*t16.*(1.6e1./1.5e1)+t2.*t3.*t4.*t5.*t15.*2.0;t38-t56+t111-t112-(t2.*t10)./1.5e1+t3.*t15.*(2.0./1.5e1)-t3.*t16.*(2.0./1.5e1)-t2.*t10.*t15.*(8.0./1.5e1)+t2.*t10.*t16.*(1.6e1./1.5e1)+t2.*t11.*t15.*(8.0./1.5e1)+(t3.*t10.*t15)./1.5e1-t2.*t11.*t16.*(1.6e1./1.5e1)-(t3.*t10.*t16)./1.5e1-t3.*t11.*t15.*(2.0./1.5e1)+t3.*t11.*t16.*(2.0./1.5e1)+t3.*t15.*t33.*(2.0./1.5e1)-t3.*t16.*t33.*(2.0./1.5e1)+t2.*t10.*t57.*(4.0./1.5e1)-t2.*t11.*t57.*(4.0./1.5e1)-t3.*t10.*t11.*t15.*(8.0./1.5e1)+t3.*t10.*t11.*t16.*(8.0./1.5e1)-t2.*t10.*t15.*t16.*(1.6e1./1.5e1)+t2.*t11.*t15.*t16.*(1.6e1./1.5e1)+t2.*t4.*t5.*t10.*t15.*4.0;t7.*(-3.0./3.2e1)+cos(th1.*6.0)./3.2e1+(t7.*cos(th2.*4.0))./1.6e1+(t3.*t4.*t7.*t12)./2.0;-t61-t113+t114-(t3.*t15)./1.5e1-(t3.*t10.*t15)./3.0e1+(t3.*t10.*t16)./3.0e1+(t3.*t11.*t15)./1.5e1-(t3.*t11.*t16)./1.5e1+(t3.*t15.*t16)./2.0+(t3.*t15.*t33)./1.0e1-(t3.*t16.*t33)./1.0e1+(t3.*t15.*t57)./6.0-t3.*t16.*t57.*(5.0./6.0)-t3.*t10.*t11.*t15.*(2.0./5.0)+t3.*t10.*t11.*t16.*(2.0./5.0)+t2.*t4.*t5.*t10.*t15.*2.0;t7.*t9.*t115.^2.*(-1.0./8.0)];
