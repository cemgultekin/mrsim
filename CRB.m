function  [F,c,cx,cy]=CRB(x,y,correlate,w,modifier)
sinx=sin(x(:,1));
y=y(:,2:end-1);
m=size(y,2);
Y=sinx.*(y(correlate,:).');
I=find(w~=0);
F=Y'*Y;

%{
freqnum=20;
N=size(Y,1);
LPF=zeros(N,2*freqnum+1);
LPF(:,1)=1;
for j=1:freqnum
    LPF(:,2*j)=sin(j*(0:N-1)/N*2*pi);
    LPF(:,2*j+1)=cos(j*(0:N-1)/N*2*pi);
end
[LPF,~]=qr(LPF,0);
%}
[c,cY]=MRsim.natural_crb(Y,I,w,modifier);
cy=sinx.*cY;
cy=cy.';
cosx=cos(x(:,1));
cosxy=cosx.*(y(correlate,:).');
cx=sum(cosxy.*cY,2);
end
%{
function  [F,c,cx,cy,cxx,cyx,cyy]=CRB(x,y,correlate,w)
sinx=sin(x(:,1));
X=diag(sinx);
y=y(:,2:end-1);
m=size(y,2);
Y=y(correlate,:).';
n=size(Y,2);
W=diag(w);

X2=X^2;
F=Y.'*X2*Y;%+1e-3*eye(size(Y,2));
VW=F\W;
c=sum(diag(VW));
if nargout<=2
    return;
end
dX=cos(x(:,1));
S=(F\VW.').'; % inv(F)*W*inv(F)
c0x=-2*diag(X*Y*S*Y.');
cx=c0x.*dX;

cy=-2*X2*Y*S;
cy=cy.';
if nargout<=4
    return;
end
ddX=-X;
V=inv(F);
YS=Y*S;
YSY=YS*Y.';
YV=Y*V;
YVY=YV*Y.';


cxx=4*(X*Y*V*Y.').*(YSY*X)+4*(X*YSY).*(Y*V*Y.'*X)-2*diag(diag(YSY));
cxx=cxx.*(dX*dX.');
cxx=cxx+c0x.*ddX;

T=Y*V*Y.'*X2;
T(1:m+1:end)=T(1:m+1:end)-1;


cyx=2*bsxfun(@times,X*YV,reshape(YSY*X2,m,1,m));
cyx=cyx+2*bsxfun(@times, YS,reshape(X*T,m,1,m));
cyx=cyx+2*bsxfun(@times, X*YS,reshape(T,m,1,m));
cyx=cyx+2*bsxfun(@times, YV, reshape(X*YSY*X2,m,1,m));
cyx=permute(cyx,[2,3,1]);
cyx=bsxfun(@times,cyx,reshape(dX,1,1,[]));

cyy=tenmultip(X2,S);
cyy=cyy-tenmultip(X2*YVY*X2,S);
cyy=cyy-tenmultip(X2*YSY*X2,V);
cyy=permute(cyy,[1,3,2,4]);
cyy=cyy-tenmultip(X2*YV,X2*YS);
cyy=cyy-tenmultip(X2*YS,X2*YV);
cyy=-2*permute(cyy,[4,1,2,3]);
end
function C=tenmultip(A,B)
C=bsxfun(@times,A,reshape(B,1,1,size(B,1),size(B,2)));
end
%}
