function [M,err,...
    Mth1,Mth2,MTRF,...
    Mth1th1,Mth1th2,Mth1TRF,Mth2th2,Mth2TRF,MTRFTRF]=multiStepType1(th1,th2,numstep,degree, TRF,m0s,T1,T2f,R,T2s,TR,errmes)
%{
if th2-th1>0
    numstep=ceil((th2-th1)/pi*numstep);
else
    numstep=1;
end
%}
th=linspace(th1,th2,numstep+1);
err=0;
M=eye(18);
if nargout>5
    Mth1=zeros(18);
    Mth2=zeros(18);
    MTRF=zeros(18);
    
    Mth1th1=zeros(18);
    Mth1th2=zeros(18);
    Mth1TRF=zeros(18);

    Mth2th2=zeros(18);
    Mth2TRF=zeros(18);
    
    MTRFTRF=zeros(18);
elseif nargout>2
    Mth1=zeros(18);
    Mth2=zeros(18);
    MTRF=zeros(18);
end
for k=1:numstep
    if nargout>5
        [M_,err_,Mth1_,Mth2_,MTRF_,...
            Mth1th1_,Mth1th2_,Mth1TRF_,Mth2th2_,Mth2TRF_,MTRFTRF_]=...
            MRsim.intervalType1(th1,th2,th(k),th(k+1),TRF,m0s,T1,T2f,R,T2s,TR,degree,errmes);
        
        Mth1th1=Mth1th1_*M+2*Mth1_*Mth1+M_*Mth1th1;
        Mth1th2=Mth1th2_*M+Mth2_*Mth1+Mth1_*Mth2+M_*Mth1th2;
        Mth1TRF=Mth1TRF_*M+MTRF_*Mth1+Mth1_*MTRF+M_*Mth1TRF;
        
        Mth2th2=Mth2th2_*M+2*Mth2_*Mth2+M_*Mth2th2;
        Mth2TRF=Mth2TRF_*M+Mth2_*MTRF+MTRF_*Mth2+M_*Mth2TRF;
        
        MTRFTRF=MTRFTRF_*M+2*MTRF_*MTRF+M_*MTRFTRF;
        
        Mth1=Mth1_*M+M_*Mth1;
        Mth2=Mth2_*M+M_*Mth2;
        MTRF=MTRF_*M+M_*MTRF;
        
        M=M_*M;
    elseif nargout>2
        [M_,err_,Mth1_,Mth2_,MTRF_]=MRsim.intervalType1(th1,th2,th(k),th(k+1),TRF,m0s,T1,T2f,R,T2s,TR,degree,errmes);
        Mth1=Mth1_*M+M_*Mth1;
        Mth2=Mth2_*M+M_*Mth2;
        MTRF=MTRF_*M+M_*MTRF;
        M=M_*M;
    else
        [M_,err_]=MRsim.intervalType1(th1,th2,th(k),th(k+1),TRF,m0s,T1,T2f,R,T2s,TR,degree,errmes);
        M=M_*M;
    end
    if errmes
        err=err+err_;
    end
end
end