function [M,err]=...
    fast_intervalType1(th1,th2,th_1,th_2,TRF,m0s,T1,T2f,R,T2s,TR,degree,errmes)
n=3;
err=0;
A=MRsim.M1fun(th1,th2,TRF,m0s,T1,T2f,R,T2s,TR); % (n,n,3)
A=A(1:n,1:n,:);
switch degree
    case 6
        B=MRsim.Bfun6(th_1,th_2);
    case 5
        B=MRsim.Bfun5(th_1,th_2);
    case 4
        B=MRsim.Bfun4(th_1,th_2);
    case 3
        B=MRsim.Bfun3(th_1,th_2);
    case 2
        B=MRsim.Bfun2(th_1,th_2);
    case 1
        B=MRsim.Bfun1(th_1,th_2);
end
B=reshape(B,1,1,1,[]);
ii=[0,cumsum(3.^(1:degree))];
A=reshape(A,1,n,n,1,[]);
X=cell(degree+1,1);
X{1}=eye(n);
for k=1:degree
    X{k+1}=reshape(sum(bsxfun(@times, X{k},A),2),n,n,1,[]);
end
M=X{1};

for k=1:degree
    XBK=bsxfun(@times,X{k+1},B(ii(k)+1:ii(k+1)));
    M=M+(-1)^k*sum(XBK,4);
end
M=inv(M);
end