function [M,err]=fast_multiStepType1(th1,th2,numstep,degree, TRF,m0s,T1,T2f,R,T2s,TR,errmes)
numstep=floor((th2-th1)/pi*numstep);
th=linspace(th1,th2,numstep+2);
err=0;
M=eye(3);

for k=1:length(th)-1
    [M_,~]=MRsim.fast_intervalType1(th1,th2,th(k),th(k+1),TRF,m0s,T1,T2f,R,T2s,TR,degree,errmes);
    M=M_*M;
end
end