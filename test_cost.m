function [F,c,dcdx,dcdy]=test_cost(theta,y,correlate,weights,TR,RelTol,AbsTol)
weights=reshape(weights,[],1);
r=@(t) MTsim.multi_eval_fun(t,y,1);
N=length(theta);
Tmax=N*TR;
x1=@(t) theta(mod(floor(t/TR),N)+1);
c=integral(@(t) r(t).^2.*sin(x1(t)).^2,0,Tmax,'ArrayValued',true,...
        'RelTol',RelTol,'AbsTol',AbsTol);
F=c;

if nargout==2
    return;
end

dcdx=@(t) r(t).^2.*sin(2*x1(t));
if nargout==3
    return;
end
y0=y(0);
dcdy=@(t) [2*r(t).*sin(x1(t)).^2;zeros(length(y0)-1,1)];
end
