function graddesc=hessianHelper(i,j,xtype1,xtype2)

if abs(i-j)>1
    graddesc=[];
    return;
end
if i~=j || xtype1~=xtype2
    %1  2   3   4    5     6       7      8      9      10
    %1 th1 th2 TRF th1th1 th1th2 th1TRF th2th2 th2TRF TRFTRF
    if xtype1==1 && xtype2==1       
             graddesc=[max(i,j),6]; %OK
             return;
    elseif xtype1==1 && xtype2==2    
            if i==j
                graddesc=[j+1,7];
            elseif i==j+1
                graddesc=[j+1,9];
            else
                graddesc=[];
            end
            return;
       elseif xtype1==2 && xtype2==1
            if i==j
                graddesc=[i+1,7];
            elseif j==i+1
                graddesc=[i+1,9];
            else
                graddesc=[];
            end
            return;
       elseif xtype1==2 && xtype2==2 
            graddesc=[];
            return;
    end
else
    if xtype1==1
        graddesc=[i,8; i+1, 5]; %OK
    else
        graddesc=[i+1, 10];
    end
    return;
end
end