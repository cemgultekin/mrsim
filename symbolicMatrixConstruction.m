function symbolicMatrixConstruction()
%%Non-transitional matrix
syms m0s T1 T2f R T2s_ TR theta TRF TRFmax
A=[-cos(theta)^2*(1/T1+R*m0s) - sin(theta)^2/T2f,               R*cos(theta)*(1-m0s), (1-m0s)*cos(theta)/T1;
                    R*cos(theta)*m0s                , - (1/T1 + R*(1-m0s)), m0s/T1 ;
                    0                               ,           0                     , 0];
ms=size(A,1);
fischerCorrelate=(1:6);
statePairs=fischerCorrelate;
if isempty(find(statePairs==1))
    statePairs=[1;statePairs];
end
numst=length(statePairs);
M=[A,zeros(ms,ms*(numst-1))];
params='M  ';
for k=2:length(statePairs)
    i=statePairs(k);
    if i==2
        D=diff(A,m0s)*m0s;
        params=[params,':  m0s  '];
    elseif i==3
        D=diff(A,T1)*T1;
        params=[params,':  T1  '];
    elseif i==4
        D=diff(A,T2f)*T2f;
        params=[params,':  T2f  '];
    elseif i==5
        D=diff(A,R)*R;
         params=[params,':  R  '];
    elseif i==6
        D=diff(A,T2s_)*T2s_;
        params=[params,':  T2s'];
    else
        error('coding error');
    end
    M=[M;D,zeros(ms,ms*(k-2)),A,zeros(ms,ms*numst-ms*k)];
end
disp(['ODE states=  ',params]);

Mth=diff(M,theta);
MTRF=diff(M,TRF);
Mth2=diff(Mth,theta);
MTRF2=diff(MTRF,TRF);
MthTRF=diff(Mth,TRF);
matlabFunction(M,'File','+MRsim/M2fun','Vars',[theta,TRF,m0s,T1,T2f,R,T2s_,TR]);
matlabFunction(Mth,'File','+MRsim/M2thfun','Vars',[theta,TRF,m0s,T1,T2f,R,T2s_,TR]);
matlabFunction(MTRF,'File','+MRsim/M2TRFfun','Vars',[theta,TRF,m0s,T1,T2f,R,T2s_,TR]);
matlabFunction(MthTRF,'File','+MRsim/M2thTRFfun','Vars',[theta,TRF,m0s,T1,T2f,R,T2s_,TR]);
matlabFunction(MTRF2,'File','+MRsim/M2TRF2fun','Vars',[theta,TRF,m0s,T1,T2f,R,T2s_,TR]);
matlabFunction(Mth2,'File','+MRsim/M2th2fun','Vars',[theta,TRF,m0s,T1,T2f,R,T2s_,TR]);


attenuation= (pi)^2 * T2s_ / TRFmax;
boundary1=diag(   [1,                 1,0]);
boundary0=diag(1./[1,-exp(- attenuation),1]);
boundaryv=        [0,                 0,1];

M=boundary0;
params='M  ';
for k=2:length(statePairs)
    i=statePairs(k);
    if i==2
        D=diff(boundary0,m0s)*m0s;
        params=[params,':  m0s  '];
    elseif i==3
        D=diff(boundary0,T1)*T1;
        params=[params,':  T1  '];
    elseif i==4
        D=diff(boundary0,T2f)*T2f;
        params=[params,':  T2f  '];
    elseif i==5
        D=diff(boundary0,R)*R;
         params=[params,':  R  '];
    elseif i==6
        D=diff(boundary0,T2s_)*T2s_;
        params=[params,':  T2s'];
    else
        error('coding error');
    end
    M=[M;D];
end
M=[M;boundary1;boundaryv];
matlabFunction(M,'File','+MRsim/boundaryfun','Vars',[m0s,T1,T2f,R,T2s_,TR,TRFmax]);




%{
%%
%Transitional Matrix

clearvars -except ODEmat

fischerCorrelate=(1:6).';
syms m0s T1 T2f R T2s_ TR th1 th2 TRF
A0=TRF/(-th1+th2)*[-(1/T1+R*m0s+1/T2f)/2,    0, 0;
                    0                , - (1/T1 + R*(1-m0s) + ((-th1+th2)/TRF)^2*T2s_), m0s/T1 ;
                    0                               ,           0                     , 0];
A1=TRF/(-th1+th2)*[0,    R*(1-m0s), (1-m0s)/T1;
                R*m0s                , 0, 0 ;
                0     ,           0, 0];
A2=TRF/(-th1+th2)*[-(1/T1+R*m0s-1/T2f)/2,   0,0;
                            0,0,0 ;
                            0 ,0,0];
A=[A0,A1,A2];
A=reshape(A,3,3,3);
ms=size(A,1);
statePairs=fischerCorrelate;
if isempty(find(statePairs==1))
    statePairs=[1;statePairs];
end
numst=length(statePairs);
M0=[A0,zeros(ms,ms*(numst-1))];
M1=[A1,zeros(ms,ms*(numst-1))];
M2=[A2,zeros(ms,ms*(numst-1))];
for k=2:length(statePairs)
    i=statePairs(k);
    if i==2
        D=diff(A,m0s)*m0s;
    elseif i==3
        D=diff(A,T1)*T1;
    elseif i==4
        D=diff(A,T2f)*T2f;
    elseif i==5
        D=diff(A,R)*R;
    elseif i==6
        D=diff(A,T2s_)*T2s_;
    end
    M0=[M0;D(:,:,1),zeros(ms,ms*(k-2)),A0,zeros(ms,ms*numst-ms*k)];
    M1=[M1;D(:,:,2),zeros(ms,ms*(k-2)),A1,zeros(ms,ms*numst-ms*k)];
    M2=[M2;D(:,:,3),zeros(ms,ms*(k-2)),A2,zeros(ms,ms*numst-ms*k)];
end


M=[M0,M1,M2];
X1=[M0;M1;M2];

M=cat(3,M0,M1,M2);

Mth1=diff(M,th1);
Mth2=diff(M,th2);
MTRF=diff(M,TRF);

Mth1th1=diff(Mth1,th1);
Mth2th2=diff(Mth2,th2);
MTRFTRF=diff(MTRF,TRF);

Mth1th2=diff(Mth1,th2);
Mth1TRF=diff(Mth1,TRF);
Mth2TRF=diff(Mth2,TRF);

matlabFunction(M,'File','M1fun','Vars',[th1,th2,TRF,m0s,T1,T2f,R,T2s_,TR]);

matlabFunction(Mth1,'File','M1th1fun','Vars',[th1,th2,TRF,m0s,T1,T2f,R,T2s_,TR]);
matlabFunction(Mth2,'File','M1th2fun','Vars',[th1,th2,TRF,m0s,T1,T2f,R,T2s_,TR]);
matlabFunction(MTRF,'File','M1TRFfun','Vars',[th1,th2,TRF,m0s,T1,T2f,R,T2s_,TR]);

matlabFunction(Mth1th1,'File','M1th1th1fun','Vars',[th1,th2,TRF,m0s,T1,T2f,R,T2s_,TR]);
matlabFunction(Mth2th2,'File','M1th2th2fun','Vars',[th1,th2,TRF,m0s,T1,T2f,R,T2s_,TR]);
matlabFunction(MTRFTRF,'File','M1TRFTRFfun','Vars',[th1,th2,TRF,m0s,T1,T2f,R,T2s_,TR]);

matlabFunction(Mth1th2,'File','M1th1th2fun','Vars',[th1,th2,TRF,m0s,T1,T2f,R,T2s_,TR]);
matlabFunction(Mth1TRF,'File','M1th1TRFfun','Vars',[th1,th2,TRF,m0s,T1,T2f,R,T2s_,TR]);
matlabFunction(Mth2TRF,'File','M1th2TRFfun','Vars',[th1,th2,TRF,m0s,T1,T2f,R,T2s_,TR]);


A=[1;cos(th2);cos(2*th2)].';
tic;
disp('B1');
B1=int(A.',th2,th1,th2);
toc;
B2=B1*A;
B2=reshape(B2.',[],1);
tic;
disp('B2');
B2=int(B2,th2,th1,th2);
toc;
B3=B2*A;
B3=reshape(B3.',[],1);
tic;
disp('B3');
B3=int(B3,th2,th1,th2);
toc;
B4=B3*A;
B4=reshape(B4.',[],1);
tic;
disp('B4');
B4=int(B4,th2,th1,th2);
toc;
B5=B4*A;
B5=reshape(B5.',[],1);
tic;
disp('B5');
B5=int(B5,th2,th1,th2);
toc;
B=[B1;B2;B3;B4;B5];

Bth1=diff(B,th1);
Bth2=diff(B,th2);

Bth1th2=diff(Bth1,th2);
Bth1th1=diff(Bth1,th1);
Bth2th2=diff(Bth2,th2);

tic;
disp('Bfun');
matlabFunction(B,'File','Bfun5','Vars',[th1,th2]);
toc;
tic;
disp('Bth1fun');
matlabFunction(Bth1,'File','Bth1fun5','Vars',[th1,th2]);
toc;
tic;
disp('Bth2fun');
matlabFunction(Bth2,'File','Bth2fun5','Vars',[th1,th2]);
toc;

tic;
disp('Bth1th1fun');
matlabFunction(Bth1th1,'File','Bth1th1fun5','Vars',[th1,th2]);
toc;
tic;
disp('Bth1th2fun');
matlabFunction(Bth1th2,'File','Bth1th2fun5','Vars',[th1,th2]);
toc;
tic;
disp('Bth2th2fun');
matlabFunction(Bth2th2,'File','Bth2th2fun5','Vars',[th1,th2]);
toc;

%}
end