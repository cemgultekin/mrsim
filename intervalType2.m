function [M,err,Mth,MTRF,Mthth,MthTRF,MTRFTRF]=...
    intervalType2(theta,TRF,m0s,T1,T2f,R,T2s,TR,errmes)
A=MRsim.M2fun(theta,TRF,m0s,T1,T2f,R,T2s,TR);
n=size(A,1);
T=(TR-TRF)/2;
err=0;
if nargout>4
    Ath=MRsim.M2thfun(theta,TRF,m0s,T1,T2f,R,T2s,TR);
    Ath2=MRsim.M2th2fun(theta,TRF,m0s,T1,T2f,R,T2s,TR);
    AA=[A,zeros(n,2*n);...
        Ath,A,zeros(n);...
        Ath2,2*Ath,A];
    M=expm(AA*T);
    Mth=M(n+1:2*n,1:n);
    Mthth=M(2*n+1:end,1:n);
    M=M(1:n,1:n);
elseif nargout>2 
    Ath=MRsim.M2thfun(theta,TRF,m0s,T1,T2f,R,T2s,TR);
    AA=[A,zeros(n);Ath,A];
    M=expm(AA*T);
    Mth=M(n+1:end,1:n);
    M=M(1:n,1:n);
else
    M=expm(A*T);
    return;
end
if TRF>0
    MTRF=-1/2*A*M;
    if nargout>4
        MTRFTRF=-1/2*A*MTRF;
        MthTRF=-1/2*(Ath*M+A*Mth);
    end
else
    MTRF=M.*0;
    MTRFTRF=MTRF;
    MthTRF=MTRF;
end
end