function [c,cy,r]=natural_crb(Y,I,W,modifier)
%f=@(r) sum(r.^2,1);
%df=@(r) 2*r;
n=size(Y,1);
m=size(Y,2);
c=0;
if nargout>1
    cy=zeros(n,m);
end
for j=1:length(I)
    i=I(j);
    yi=Y(:,i);
    Yi=Y(:,[1:i-1,i+1:size(Y,2)]);
    [Q,~]=qr(Yi,0);
    P=Q*Q.';
    r=yi-(Q*Q.'*yi);
    if isempty(modifier)
        c1=1/sum( (r*W(I(j))).^2);
    else
    %elseif LOGFLAG==0
        c1=1/sum(pnorm(r*W(I(j)),modifier.p,modifier.delta));
    %else
        
    end
    
    c=c+c1;
    
    if nargout>1
        cy1=cy.*0;
        if isempty(modifier)
            dfdr=-c1^2.*2*r*W(I(j))^2;
        else
        %elseif LOGFLAG==0
            dfdr=-c1^2*dpnorm(r*W(I(j)),modifier.p,modifier.delta)*W(I(j));
        %else
            
        end
        u=yi;v=dfdr;
        [~,gi]=projection_derivative_(Yi,u,v);
        cy1(:,[1:i-1,i+1:size(Y,2)])=-gi;
        cy1(:,i)=-P*dfdr+dfdr;
        cy=cy+cy1;
    end
end
end

function h=pseudo_huber_loss(a,delta)
h=delta*(sqrt(1+(a/delta).^2)-1);
end
function h=dpseudo_huber_loss(a,delta)
h=a./sqrt(1+a.^2/delta^2)./delta;
end
function h=pnorm(x,p,delta)
h=sum((x.^2+delta^2).^(p/2)-delta^p)^(1/p);
end
function h=dpnorm(x,p,delta)
x2d2=(x.^2+delta^2);
h=sum(x2d2.^(p/2)-delta^p)^(1/p-1);
h=x.*x2d2.^(p/2-1)*h;
end

function [c,cy]=projection_derivative_(y,u,v)
[Q,~]=qr(y,0);
P=Q*Q';
c=v'*P*u;
guv=(y'*y)\(y'*[u,v]);
cy=(u-P*u)*guv(:,2).'+(v-P*v)*guv(:,1).';
end
