classdef Simulator < handle
   properties(Access = public) 
       M
       m0s
       T1
       T2f
       R
       T2s
       TR
       IVP
       Tmax
       trfmax
       fischerCorrelate
       odeMat
       odeVect
       matcollect
       timestep
       odeSolverOrder
       dictionaryMode
       matsize
       bound0
       bound1
       boundv
       optdesign
       fval
       dfdx
       dfdy
       state
       adjoint
       fingerprints
       control
       fischermat
       weights
       ptsnum
       CRBmodification
   end
   methods
      function obj=Simulator(varargin)
         defaultM=1.0;
         defaultm0s=.1;%0.3;%
         defaultT1=1.;%0.7810;%
         defaultT2f=.075;%.065;%
         defaultR=15;%30;30;%
         defaultT2s=44e-6;%10e-6;10e-6;%
         defaultTR=4.5e-3;
         defaultTmax=3;
         defaultFischerCorrelate=(1:6).';
         defaultIVP=false;
         defaultOptDesign=0;
         defaultTimeStep=64;
         defaultDictionaryMode=0;
         defaultodeSolverOrder=2;
         defaultCRBmodification=[];
         load('./+MRsim/defaultControl','defaultControl');
         defaultTRFmax=1e-3;%pi^2 * defaultT2s/
         defaultWeights=[0,1,1,1,0,0].';
         validFischer= @(x) isnumeric(x) && max(x<=6) && min(x>=1);
         validBool = @(x) islogical(x) || isscalar(x);
         validScalarPosNum = @(x) isnumeric(x) && isscalar(x) && (x >= 0);
         p=inputParser;
         p.CaseSensitive=false;
         addOptional(p,'M',defaultM,validScalarPosNum);
         addOptional(p,'m0s',defaultm0s,validScalarPosNum);
         addOptional(p,'T1',defaultT1,validScalarPosNum);
         addOptional(p,'T2f',defaultT2f,validScalarPosNum);
         addOptional(p,'R',defaultR,validScalarPosNum);
         addOptional(p,'T2s',defaultT2s,validScalarPosNum);
         addOptional(p,'TRFmax',defaultTRFmax,validScalarPosNum);
         addOptional(p,'IVP',defaultIVP,validBool);
         addOptional(p,'TR',defaultTR,validScalarPosNum);
         addOptional(p,'FischerCorrelate',defaultFischerCorrelate,validFischer);
         addOptional(p,'Weights',defaultWeights,@(x) isnumeric(x));
         addOptional(p,'Control',defaultControl);
         addOptional(p,'Tmax',defaultTmax,validScalarPosNum);
         addOptional(p,'OptDesign',defaultOptDesign,validBool);
         addOptional(p,'TimeStep',defaultTimeStep,validScalarPosNum);
         addOptional(p,'OdeSolverOrder',defaultodeSolverOrder,validScalarPosNum);
         addOptional(p,'DictionaryMode',defaultDictionaryMode,validBool);
         addOptional(p,'CRBmodification',defaultCRBmodification);
         parse(p,varargin{:});
         obj.M=p.Results.M;
         obj.m0s=p.Results.m0s;
         obj.T1=p.Results.T1;
         obj.T2f=p.Results.T2f;
         obj.R=p.Results.R;
         obj.T2s=p.Results.T2s;
         obj.TR=p.Results.TR;
         obj.IVP=p.Results.IVP;
         obj.weights=reshape(p.Results.Weights,[],1);
         obj.fischerCorrelate=reshape(p.Results.FischerCorrelate,[],1);
         obj.Tmax=p.Results.Tmax;
         obj.ptsnum=floor(obj.Tmax/obj.TR)+2;
         obj.Tmax=(obj.ptsnum-2)*obj.TR;
         obj.control=p.Results.Control;
         obj.optdesign=p.Results.OptDesign;
         obj.timestep=p.Results.TimeStep;
         obj.odeSolverOrder=p.Results.OdeSolverOrder;
         obj.dictionaryMode=p.Results.DictionaryMode;
         obj.CRBmodification=p.Results.CRBmodification;
         if sum(strcmp(p.UsingDefaults,'TRFmax'))>0
             obj.trfmax=1e-3;%pi^2 * obj.T2s/
         else
             obj.trfmax=p.Results.TRFmax;
         end
         if obj.ptsnum-2~=size(obj.control,1)
             tt=linspace(0,obj.Tmax,obj.ptsnum-2);
             x=zeros(obj.ptsnum-2,2);
             x(:,1)=MRsim.hann_interpolation(tt,obj.Tmax,obj.control(:,1));
             x(:,2)=MRsim.hann_interpolation(tt,obj.Tmax,obj.control(:,2));
             obj.control=x;
         end
        if obj.dictionaryMode
            numofstates=3;
            obj.matsize=3;
            m0s=obj.m0s;
            y0=[m0s;m0s-1;1];
            bound0=eye(numofstates);
            if obj.IVP  
                bound1=zeros(size(bound0));
                boundv=y0;
            else
                bound1=ones(numofstates,1);
                bound1(2)=-1/(1-obj.trfmax);
                bound1(3)=0;
                bound1=diag(bound1);
                boundv=zeros(numofstates,1);
                boundv(3)=1;
            end
            obj.bound0=bound0;
            obj.bound1=bound1;
            obj.boundv=boundv;
            obj.odeVect=[boundv;zeros(length(boundv)*(obj.ptsnum-1),1)];
        else
            states=obj.fischerCorrelate;
            if isempty(find(states==1, 1))
                states=[1;states];
            end
            states=sort([3*states-1;3*states-2;3*states]);
            numofstates=length(states);
            obj.matsize=numofstates;
            m0s=obj.m0s;
            y0=[m0s;m0s-1;1;1;zeros(14,1)];
            y0=y0(states);
            bound0=eye(numofstates);
            if obj.IVP  
                bound1=zeros(size(bound0));
                boundv=y0;
            else
                bound1=ones(numofstates,1);
                bound1(2:3:end)=-(1-pi^2 * obj.T2s/obj.trfmax);%-1/(1-pi^2 * obj.T2s/obj.trfmax);
                bound1(3:3:end)=0;
                bound1=diag(bound1);
                bound1(end-1,2)=pi^2 * obj.T2s/obj.trfmax;%-obj.T2s*pi^2/obj.trfmax/(1-pi^2 * obj.T2s/obj.trfmax)^2;%;
                boundv=zeros(numofstates,1);
                boundv(3)=1;
            end
            obj.bound0=bound0;
            obj.bound1=bound1;
            obj.boundv=boundv;
            obj.odeVect=[boundv;zeros(length(boundv)*(obj.ptsnum-1),1)];
        end
      end
      
      function setParameters(obj,varargin)
         defaultM=obj.M;
         defaultm0s=obj.m0s;
         defaultT1=obj.T1;
         defaultT2f=obj.T2f;
         defaultR=obj.R;
         defaultT2s=obj.T2s;
         defaultControl=obj.control;
         defaultWeights=obj.weights;
         defaultTR=obj.TR;
         defaultTRFmax=obj.trfmax;
         defaultTimeStep=obj.timestep;
         defaultodeSolverOrder=obj.odeSolverOrder;
         defaultDictionaryMode=obj.dictionaryMode;
         validBool = @(x) islogical(x) || isscalar(x);
         validScalarPosNum = @(x) isnumeric(x) && isscalar(x) && (x >= 0);
         p=inputParser;
         p.CaseSensitive=false;
         addOptional(p,'OdeSolverOrder',defaultodeSolverOrder,validScalarPosNum);
         addOptional(p,'TimeStep',defaultTimeStep,validScalarPosNum);
         addOptional(p,'M',defaultM,validScalarPosNum);
         addOptional(p,'m0s',defaultm0s,validScalarPosNum);
         addOptional(p,'T1',defaultT1,validScalarPosNum);
         addOptional(p,'T2f',defaultT2f,validScalarPosNum);
         addOptional(p,'TRFmax',defaultTRFmax,validScalarPosNum);
         addOptional(p,'R',defaultR,validScalarPosNum);
         addOptional(p,'T2s',defaultT2s,validScalarPosNum);
         addOptional(p,'TR',defaultTR,validScalarPosNum);
         addOptional(p,'Control',defaultControl, @(x) size(x,2)==2);
         addOptional(p,'Weights',defaultWeights, @(x) isnumeric(x) && length(x)==length(defaultWeights));
         addOptional(p,'DictionaryMode',defaultDictionaryMode,validBool);
         parse(p,varargin{:});
         obj.timestep=p.Results.TimeStep;
         obj.odeSolverOrder=p.Results.OdeSolverOrder;
         obj.M=p.Results.M;
         obj.m0s=p.Results.m0s;
         obj.T1=p.Results.T1;
         obj.T2f=p.Results.T2f;
         obj.R=p.Results.R;
         obj.T2s=p.Results.T2s;
         obj.control=p.Results.Control;
         obj.weights=p.Results.Weights;
         obj.TR=p.Results.TR;
         obj.trfmax=p.Results.TRFmax;
         obj.dictionaryMode=p.Results.DictionaryMode;
         changeflag=false;
         if obj.T2s~=defaultT2s || obj.trfmax ~= defaultTRFmax
             obj.trfmax=pi^2 * obj.T2s/1e-3;
             numofstates=obj.matsize;
                obj.bound1=ones(numofstates,1);
                obj.bound1(2:3:end)=-(1-pi^2 * obj.T2s/obj.trfmax);
                obj.bound1(3:3:end)=0;
                obj.bound1=diag(obj.bound1);
                if ~obj.dictionaryMode
                   obj.bound1(end-1,2)=pi^2 * obj.T2s/obj.trfmax;
                end
                changeflag=true;
         end
         if ~changeflag && obj.m0s~=defaultm0s
             changeflag=true;
         elseif ~changeflag && obj.T1~=defaultT1
             changeflag=true;
         elseif ~changeflag && obj.T2f~=defaultT2f
             changeflag=true;
         elseif ~changeflag && obj.R~=defaultR
             changeflag=true;
         elseif ~changeflag && obj.T2s~=defaultT2s
             changeflag=true;
         elseif ~changeflag && ~isequal(obj.control,defaultControl)
             changeflag=true;
         elseif ~changeflag && obj.timestep~=defaultTimeStep
             changeflag=true;
         elseif ~changeflag && obj.odeSolverOrder~=defaultodeSolverOrder
            changeflag=true;
         elseif ~changeflag && obj.dictionaryMode~=defaultDictionaryMode
            changeflag=true;
         end
         if changeflag
            obj.fischermat=[];
            obj.fingerprints=[];
            obj.fval=[];
            obj.dfdx=[];
            obj.dfdy=[];
            obj.state=[];
            obj.adjoint=[];
            obj.odeMat=[];
            obj.matcollect=[];
         end
         if obj.weights~=defaultWeights
               obj.fval=[];
               obj.dfdx=[];
               obj.dfdy=[];
         end
         if obj.TR~=defaultTR 
             obj.fischermat=[];
             obj.fval=[];
             obj.dfdx=[];
             obj.dfdy=[];
         end
         if obj.M~=defaultM
               obj.fval=[];
               obj.dfdx=[];
               obj.dfdy=[];
               obj.fischermat=[];
         end
         if obj.dictionaryMode~=defaultDictionaryMode
             if obj.dictionaryMode
                numofstates=3;
                obj.matsize=3;
                m0s=obj.m0s;
                y0=[m0s;m0s-1;1];
                bound0=eye(numofstates);
                if obj.IVP  
                    bound1=zeros(size(bound0));
                    boundv=y0;
                else
                    bound1=ones(numofstates,1);
                    bound1(2)=-(1-pi^2 * obj.T2s/obj.trfmax);
                    bound1(3)=0;
                    bound1=diag(bound1);
                    boundv=zeros(numofstates,1);
                    boundv(3)=1;
                end
                obj.bound0=bound0;
                obj.bound1=bound1;
                obj.boundv=boundv;
                obj.odeVect=[boundv;zeros(length(boundv)*(obj.ptsnum-1),1)];
            else
                 states=(1:6).';
                if isempty(find(states==1, 1))
                    states=[1;states];
                end
                states=sort([3*states-1;3*states-2;3*states]);
                numofstates=length(states);
                obj.matsize=numofstates;
                
             end
         end
      end
      
      function MC=matrixCollect(obj,varargin)
          obj.usualInputProcess(varargin{:});
          if isempty(obj.odeMat)
              if obj.dictionaryMode
                  n=obj.matsize;
                  N=obj.ptsnum-2;
                  MC=zeros(n,n,N+1);
                  errmes=0;
                  for k=1:N+1
                     [MC(:,:,k),~]=MRsim.fast_matrixConstructionHelper(k,obj.control,...
                             obj.m0s,obj.T1,obj.T2f,obj.R,obj.T2s,obj.TR,...
                             obj.timestep,obj.odeSolverOrder,...
                             obj.optdesign,errmes);
                  end
                  MC=reshape(MC,n,1,n,N+1);
                  MC=permute(MC,[1,3,4,2]);
                  obj.matcollect=MC;
              else
                  n=obj.matsize;
                  switch obj.optdesign
                      case 2 
                          devnum=10;
                      case 1
                          devnum=4;
                      otherwise
                          devnum=1;
                  end
                  N=obj.ptsnum-2;
                  MC=zeros(n*devnum,n,N+1);
                  errmes=0;
                  parfor k=1:N+1
                     [MC(:,:,k),~]=MRsim.matrixConstructionHelper(k,obj.control,...
                             obj.m0s,obj.T1,obj.T2f,obj.R,obj.T2s,obj.TR,...
                             obj.timestep,obj.odeSolverOrder,...
                             obj.optdesign,errmes);
                  end
                  MC=reshape(MC,n,devnum,n,N+1);
                  MC=permute(MC,[1,3,4,2]);
                  obj.matcollect=MC;
              end
          else
              MC=obj.matcollect;
          end
      end
    
      function MT=evaluateConstraintMatrix(obj,varargin)
          obj.usualInputProcess(varargin{:});
          if isempty(obj.odeMat)
              obj.matrixCollect();
              n=obj.matsize;
              nblock=obj.ptsnum;
              MT=spalloc(n*nblock,n*nblock,n^2*nblock+n*nblock+1);
              
              for k=2:nblock
                MT((k-1)*n+(1:n),(k-2)*n+(1:n))=sparse(obj.matcollect(:,:,k-1,1)); 
              end
              MT=MT-speye(n*nblock);
              BB=MRsim.boundaryfun(obj.m0s,obj.T1,obj.T2f,obj.R,...
                            obj.T2s,obj.TR,obj.control(end,2));
              BB0=zeros(n);
              
              BB0(1:n,1:size(BB,2))=BB(1:n,:);
              for i=2:n/size(BB,2)
                  BB0( (i-1)*size(BB,2)+(1:size(BB,2)),...
                      (i-1)*size(BB,2)+(1:size(BB,2)))=BB(1:size(BB,2),1:size(BB,2));
              end
              BB(1:n,:)=[];
              BBv=BB(end,:);
              BB(end,:)=[];
              BB1=zeros(n);
              BB1(1:size(BB,1),1:size(BB,1))=BB;
              obj.odeVect=[BBv.';
                            zeros(n-length(BBv),1);
                            zeros((nblock-1)*n,1)];
              for i=2:n/size(BB,1)
                  BB1( (i-1)*size(BB,1)+(1:size(BB,1)),...
                      (i-1)*size(BB,1)+(1:size(BB,1)))=BB;
              end
              MT(1:n,end-n+1:end)=BB1;
              MT(1:n,1:n)=BB0;
              obj.odeMat=MT;
          else
              MT=obj.odeMat;
          end
      end
      function res=odeResidual(obj,varargin)
          y=obj.solveState();
          res=norm(obj.odeMat*y(:)-obj.odeVect);
      end
      function y=solveState(obj,varargin)
           obj.usualInputProcess(varargin{:});
           if isempty(obj.state)
               obj.evaluateConstraintMatrix();
               y=obj.odeMat\obj.odeVect;
               %norm(obj.odeMat*y-obj.odeVect)
               y=reshape(y,[],obj.ptsnum);
               
               obj.state=y;
           else
               y=obj.state;
           end
      end
      function fp=evaluateFingerprints(obj,varargin)
          obj.usualInputProcess(varargin{:});
          obj.solveState();
          y=obj.state(1:3:end,:).';
          fp=y.*sin([obj.control(1,1);obj.control(:,1);obj.control(end,1)])*obj.M;
          fp=fp(2:end-1,1);
          obj.fingerprints=fp;
      end
      function dfp=evaluateFingerprintGradient(obj,varargin)
          obj.usualInputProcess(varargin{:});
          obj.solveState();
          y=obj.state(1:3:end,:).';
          dfp=y.*sin([obj.control(1,1);obj.control(:,1);obj.control(end,1)])*obj.M;
          D=[1;obj.m0s;obj.T1;obj.T2f;obj.R;obj.T2s];
          D=D.*(D>0)+(D==0);
          D=diag(1./D);
          dfp=dfp*D;
          dfp=dfp(2:end-1,:);
          obj.fingerprints=dfp;
      end
      
      function F=evaluateFischer(obj,varargin)
          obj.usualInputProcess(varargin{:});
          if isempty(obj.fischermat)
                obj.usualInputProcess(varargin{:});
                obj.solveState();
                F=MRsim.CRB(obj.control,obj.state,...
                obj.fischerCorrelate*3-2,obj.weights);
                D=[1;obj.m0s;obj.T1;obj.T2f;obj.R;obj.T2s];
                D=D.*(D>0)+(D==0);
                D=diag(D);
                D=inv(D);
                obj.fischermat=D*F*D;
          else
              F=obj.fischermat;
          end
      end
      function C=evaluateCRB(obj,diagonals,varargin)
        obj.usualInputProcess(varargin{:});
        obj.solveState();
        F=MRsim.CRB(obj.control,obj.state,...
            obj.fischerCorrelate*3-2,obj.weights,obj.CRBmodification);
        E=zeros(size(F,1),1);
        E(diagonals)=1;
        E=diag(E);
        C=diag(E'*(F\E));
        C=C(diagonals);
        params=[obj.M,obj.m0s,obj.T1,obj.T2f,obj.R,obj.T2s].^2.';
        C=C.*params(diagonals)./obj.M^2;
      end
      function C=evaluaterelCRB(obj,diagonals,varargin)
        obj.usualInputProcess(varargin{:});
        obj.solveState();
        [F,~,~]=MRsim.CRB(obj.control,obj.state,...
            obj.fischerCorrelate*3-2,obj.weights);
        E=zeros(size(F,1),1);
        E(diagonals)=1;
        E=diag(E);
        C=diag(E'*(F\E));
        C=C(diagonals)*obj.Tmax/obj.TR;
        %params=[obj.M,obj.m0s,obj.T1,obj.T2f,obj.R,obj.T2s].^2.';
        %C=C.*params(diagonals);
      end
      
      function [fval,dfdx,dfdy]=evaluateCRBgradient(obj,varargin)
          obj.usualInputProcess(varargin{:});
          if isempty(obj.dfdx)|| isempty(obj.dfdy)
            obj.solveState();
            [F,fval,dfdx,dfdy]=MRsim.CRB(obj.control,obj.state,...
                obj.fischerCorrelate*3-2,obj.weights,obj.CRBmodification);
            obj.fischermat=F;
            obj.fval=fval;
            obj.dfdx=dfdx;
            obj.dfdy=dfdy;
          else
              fval=obj.fval;
              dfdx=obj.dfdx;
              dfdy=obj.dfdy;
          end
      end
      function p=solveAdjoint(obj,varargin)
          obj.usualInputProcess(varargin{:});
          if isempty(obj.adjoint)
            obj.solveState();
            obj.evaluateCRBgradient();
            rhs=zeros(size(obj.state));
            rhs(1:3:end,2:end-1)=obj.dfdy;
            p=-(obj.odeMat.')\rhs(:);
            p=reshape(p,obj.matsize,[]);
            obj.adjoint=p;
          else
              p=obj.adjoint;
          end
      end
      function H=optimalDesignHessian(obj,varargin)
          obj.solveAdjoint();
          [F,fval,fx,fy,Hxx,fyx,fyy]=MRsim.CRB(obj.control,obj.state,...
                obj.fischerCorrelate*3-2,obj.weights);
          n=size(fy,1);% num-param in CRB
          m=size(fy,2);% interior time indice num
          d=3;

          MC=obj.matcollect;
          y=obj.state;
          p=obj.adjoint;
            %{
          g=zeros(n*d,m+2);
          dydx=zeros(n*d*(m+2),m,2);
          %}
          y=reshape(y,1,size(y,1),[]);
          MCy3=sum(bsxfun(@times, MC(:,:,1:m,3),y(1,:,1:m)),2);
          MCy2=sum(bsxfun(@times, MC(:,:,2:m+1,2),y(1,:,2:m+1)),2);
          MCy4=sum(bsxfun(@times, MC(:,:,2:m+1,4),y(1,:,2:m+1)),2);
          %tic;
          G=spalloc(size(obj.odeMat,1),m*2,2*m*n*n*d*d);
          for j=1:m
              G( j*n*d + (1:n*d), j)=MCy3(:,1,j);
              G( (j+1)*n*d + (1:n*d), j)=MCy2(:,1,j);
              G( (j+1)*n*d + (1:n*d), j+m)=MCy4(:,1,j);
          end
          dydx=-obj.odeMat\G;
          y=reshape(y,n*d,m+2);
         
          %{
          for k=1:size(dydx,3)
                 for j=1:size(dydx,2)
                     CMi=MRsim.gradientHelper(j,k);
                     if ~isempty(CMi)
                         g=g.*0;
                         for s=1:size(CMi,1)
                             g(:,CMi(s,1)+1)=g(:,CMi(s,1)+1)+...
                                 MC(:,:,CMi(s,1),CMi(s,2))*y(:,CMi(s,1));
                         end
                          dydx(:,j,k)=-obj.odeMat\g(:);
                     end
                 end
          end
          %}
          dydx=reshape(full(dydx),n*d,[],m,2);
          sub_dydx=dydx(1:3:end,2:end-1,:,:);
          sub_dydx=reshape(sub_dydx,[],m*2);
          fyx=reshape(fyx,[],m);
          Hxy=fyx.'*sub_dydx;
          %{
          dydx=reshape(dydx,n*d,[],m,2);
          fyx=reshape(fyx,n,m,m);
          sub_dydx=dydx(1:3:end,2:end-1,:,:);
          Hxy=sum(bsxfun(@times,fyx,reshape(sub_dydx,n,m,1,m,2)),[1,2]);
          %}
          Hxy=reshape(Hxy,m,1,m,2);
          Hxy=cat(2,Hxy,Hxy.*0);
          Hxy=reshape(Hxy,2*m,2*m);
          H=Hxy+Hxy.';
          fyy=reshape(fyy,n*m,n*m);
          sub_dydx=reshape(sub_dydx,n*m,m*2);
          H=H+sub_dydx.'*fyy*sub_dydx;
          
          Mdy=squeeze(sum(bsxfun(@times, MC(:,:,:,2:4),reshape(dydx(:,1:m+1,:,:),1,n*d,m+1,1,2*m)),2));
         
          fyij1=squeeze(sum(Mdy(:,1:m,2,:).*p(:,2:m+1),1)+sum(Mdy(:,2:m+1,1,:).*p(:,3:m+2),1));
          fyij2=squeeze(sum(Mdy(:,2:m+1,3,:).*p(:,3:m+2),1));
          fyij=[fyij1;fyij2];
          %{
          
          fyij=zeros(m*2,m*2);
          dydx=reshape(dydx,d*n,m+2,m*2);
          for i=1:2*m
              for j=1:2*m
                  k1=ceil(i/m);
                  CM1=MRsim.gradientHelper(i-(k1-1)*m,k1);
                  g=0;
                  for s1=1:size(CM1,1)
                     g=g+...
                     p(:,CM1(s1,1)+1).'*...
                     MC(:,:,CM1(s1,1),CM1(s1,2))*...
                     dydx(:,CM1(s1,1),j);
                  end
                  fyij(i,j)=g;
              end
          end
           %}
          H=H+fyij+fyij.';
          fyij=zeros(m,2,m,2);
          for k1=1:2
              for k2=1:2
                  for i=1:m
                      for j=max(1,i-1):min(m,i+1)
                          CM1=MRsim.hessianHelper(i,j,k1,k2);
                          g=0;
                          for s1=1:size(CM1,1)
                              g=g+...
                             p(:,CM1(s1,1)+1).'*...
                             MC(:,:,CM1(s1,1),CM1(s1,2))*...
                             y(:,CM1(s1,1));
                          end
                          fyij(i,k1,j,k2)=g;
                      end
                  end
              end
          end
          fyij=reshape(fyij,2*m,2*m);
          H=H+fyij;
          H(1:m,1:m)=H(1:m,1:m)+Hxx;
      end
      function fx=optimalDesignGradient(obj,varargin)
          obj.usualInputProcess(varargin{:});
          obj.solveAdjoint();
          p=obj.adjoint;
          y=obj.state;
          if isempty(obj.dfdx)
              obj.evaluateCRBgradient();
          end
          fx=obj.dfdx;
          fx=[fx,zeros(size(fx))];
          MC=obj.matcollect;
          for k=1:size(fx,2)
              for i=1:size(fx,1)
                  %taking derivative of parameter type k, index i
                  %CMi=[ time indices, effective derivative index]
                    CMi=MRsim.gradientHelper(i,k);
                    g=0;
                    for j=1:size(CMi,1)
                        g=g+...
                            p(:,CMi(j,1)+1).'*...
                            MC(:,:,CMi(j,1),CMi(j,2))*...
                            y(:,CMi(j,1));
                    end
                    fx(i,k)=fx(i,k)+g;
              end
          end
      end
      
      function fval=optimalDesignCost(obj,varargin)
          obj.usualInputProcess(varargin{:});
          [fval,~,~]=obj.evaluateCRBgradient();
      end
      function [b1,b2]=magnetizationDynamics(obj,varargin)
          obj.usualInputProcess(varargin{:});
          obj.solveState();
          b=obj.state(1:3:end,:);
          theta=[obj.control(1,1);obj.control(:,1);obj.control(end,1)];
             b1=b(1,:).*sin(theta)';
             b2=b(1,:).*cos(theta)';
      end
      function rp=infosignals(obj,varargin)
          obj.usualInputProcess(varargin{:});
          dfp=obj.evaluateFingerprintGradient();
          ps=[obj.M;obj.m0s;obj.T1;obj.T2f;obj.R;obj.T2s];
          rp=zeros(size(dfp));
          for k=1:6
              s=dfp(:,k);
              y=[dfp(:,1:k-1),dfp(:,k+1:end)];
              [Q,~]=qr(y,0);
              s=s-Q*Q.'*s;
              rp(:,k)=s*ps(k);
          end
      end
      function presentResult(obj,varargin)
          obj.usualInputProcess(varargin{:});
          obj.solveState();
          b=obj.state(1:3:end,:);
          theta=[obj.control(1,1);obj.control(:,1);obj.control(end,1)];
          figure('Position',[200 200 500 500]);
          plot(b(1,:).*sin(theta)', b(1,:).*cos(theta)','LineWidth',2);
            hold on;
            plot(sin(0:.01:pi), cos(0:.01:pi), 'black');
            plot(-sin(0:.01:pi), cos(0:.01:pi), 'black');
            plot([0 0], [-1 1], 'black');
            plot([-1 1], [0 0], 'black');
            plot( sqrt(obj.T2f/obj.T1 * (1/4 - ((0:.01:1) - .5).^2)), 0:.01:1, 'red');
            plot(-sqrt(obj.T2f/obj.T1 * (1/4 - ((0:.01:1) - .5).^2)), 0:.01:1, 'red');
            xlabel('y'); ylabel('z'); axis equal;
            C=obj.evaluaterelCRB([1:6]);
           label1={'M','m0s','T1','T2f','R','T2s'};
           text(-0.9,0.1, ['rCRB:'],'FontSize',10);
           for k=1:length(label1)
               text(-0.9,0.1-k*0.15, label1{k},'FontSize',11);
               text(-0.7,0.1-k*0.15, num2str(C(k),3),'FontSize',11);
           end
           
          figure,
          dfp=obj.evaluateFingerprintGradient();
          subplot(2,2,1);
          tt=linspace(0,obj.Tmax,length(obj.control(:,1)));
          plot(tt,obj.control(:,1));
          xlabel('theta');
          xlim([-obj.Tmax/20,obj.Tmax*1.05]);
          subplot(2,2,3);
           plot(tt,obj.control(:,2));
           xlabel('TRF (sec)');
           xlim([-obj.Tmax/20,obj.Tmax*1.05]);
          ps=[obj.M;obj.m0s;obj.T1;obj.T2f;obj.R;obj.T2s];
          for k=2:4
              s=dfp(:,k);
              y=[dfp(:,1:k-1),dfp(:,k+1:end)];
              [Q,~]=qr(y,0);
              s=s-Q*Q.'*s;
              subplot(3,2,2*(k-1));
              plot(tt,s*ps(k));
              switch k
                  case 2
                      xlabel('m0s-info');
                  case 3
                      xlabel('T1-info');
                  case 4
                      xlabel('T2f-info');
              end
          end
      end
      function [abserr,relerr,dx]=testOptimalDesignGradient(obj,varargin)
          obj.usualInputProcess(varargin{:});
          x=obj.control;
          dx=randn(obj.ptsnum-2,2);
          dx(:,1)=dx(:,1);
          dx(:,2)=dx(:,2)*1e-3;
          hs=10.^(-(2:0.5:15));
          c0=obj.optimalDesignCost();
          gx=obj.optimalDesignGradient();
          dc2=sum(sum(gx.*dx));
          relerr=[];
          abserr=[];
          odeg=obj.optdesign;
          obj.optdesign=0;
          for j=1:length(hs)
                h=hs(j);
                y=obj.state;
                c1=obj.optimalDesignCost(x+h*dx);
                obj.state=y;
                dc1=(c1-c0)/h;
                relerr(j)=abs(dc2-dc1)/abs(dc2);
                abserr(j)=abs(dc2-dc1);
                str = sprintf('h= %e \t AbsErr: %e \t RelErr: %e ',h,abserr(j),relerr(j));
                disp(str);
                if relerr(end)>1e1
                    break;
                end
          end
          obj.control=x;
          obj.optdesign=odeg;
      end
      function [abserr,relerr]=testOptimalDesignHessian(obj,varargin)
          obj.usualInputProcess(varargin{:});
          x=obj.control;          
          hs=10.^(-(2:13));
          G=obj.optimalDesignGradient();
          H=obj.optimalDesignHessian();
            dx=randn(size(obj.control));
            dx(:,2)=dx(:,2)*1e-3;
            t=H*dx(:);
            relerr=zeros(length(hs),1);
            abserr=zeros(length(hs),1);
            odeg=obj.optdesign;
            obj.optdesign=1;
            for j=1:length(hs)
                h=hs(j);
                Gh=obj.optimalDesignGradient(x+h*dx);
                nt=(Gh(:)-G(:))/h;
                v=find(abs(nt)>1e-15);
                relerr(j)=mean(abs(nt(v)-t(v))./abs(nt(v)));
                abserr(j)=mean(abs(nt(v)-t(v)));
                str = sprintf('h= %e \t AbsErr: %e \t RelErr: %e ',h,abserr(j),relerr(j));
                disp(str);
            end
          obj.control=x;
          obj.optdesign=odeg;
      end
      
      function usualInputProcess(obj,varargin)
        x=[];
        vararg=varargin;
          if length(varargin)>1
            if isnumeric(varargin{1}) && ~isempty(varargin{1}) 
                if length(varargin{1}(:))==length(obj.control(:))
                    x=varargin{1};
                    vararg=cell(length(varargin)-1,1);
                    for i=1:length(vararg)
                        vararg{i}=varargin{i+1};
                    end
                end
            end
          elseif length(varargin)==1
            x=varargin{1};
            vararg={};
          else
           return;
          end
          if isempty(x)
             obj.setParameters(vararg{:});
          else
             obj.setParameters('Control',x,vararg{:});
          end
      end
   end
end
