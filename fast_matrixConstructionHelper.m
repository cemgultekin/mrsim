function [A,errs]=fast_matrixConstructionHelper(k,control,m0s,T1,T2f,R,T2s,TR,...
    type1stepnum,degree,devs,errmes)
errs=0;
if k==1
    th2=control(k,1);
    [A,errs]=MRsim.fast_intervalType2(th2,0,m0s,T1,T2f,R,T2s,TR,errmes);
    return;
end
if k==size(control,1)+1
    th1=control(k-1,1);
    [A,errs]=MRsim.fast_intervalType2(th1,0,m0s,T1,T2f,R,T2s,TR,errmes);
    return; 
end
th1=control(k-1,1);
th2=control(k,1);
TRF=control(k-1,2);



if devs==0
    [M0,err]=MRsim.fast_intervalType2(th1,TRF,m0s,T1,T2f,R,T2s,TR,errmes);
    errs=errs+err;
    [M1,err]=MRsim.fast_multiStepType1(-th1,th2,type1stepnum,degree,TRF,m0s,T1,T2f,R,T2s,TR,errmes);
    errs=errs+err;
    [M2,err]=MRsim.fast_intervalType2(th2,TRF,m0s,T1,T2f,R,T2s,TR,errmes);
    errs=errs+err;
    A=M2*M1*M0;
    return;
end
end