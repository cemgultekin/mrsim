function [M,err]=...
    fast_intervalType2(theta,TRF,m0s,T1,T2f,R,T2s,TR,errmes)
A=MRsim.M2fun(theta,TRF,m0s,T1,T2f,R,T2s,TR);
A=A(1:3,1:3);
T=(TR-TRF)/2;
err=0;
M=expm(A*T);
end