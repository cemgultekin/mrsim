function B=Bfunhelper(th_1,th_2,order,devnum)

if devnum==0
    switch degree
    case 6
        B=MRsim.Bfun6(th_1,th_2);
    case 5
        B=MRsim.Bfun5(th_1,th_2);
    case 4
        B=MRsim.Bfun4(th_1,th_2);
    case 3
        B=MRsim.Bfun3(th_1,th_2);
    case 2
        B=MRsim.Bfun2(th_1,th_2);
    case 1
        B=MRsim.Bfun1(th_1,th_2);
    end
else
    switch order
        case 6
            Bth1_=MRsim.Bth1fun6(th_1,th_2);
            Bth2_=MRsim.Bth2fun6(th_1,th_2);
        case 5
            Bth1_=MRsim.Bth1fun5(th_1,th_2);
            Bth2_=MRsim.Bth2fun5(th_1,th_2);
        case 4
            Bth1_=MRsim.Bth1fun4(th_1,th_2);
            Bth2_=MRsim.Bth2fun4(th_1,th_2);
        case 3
            Bth1_=MRsim.Bth1fun3(th_1,th_2);
            Bth2_=MRsim.Bth2fun3(th_1,th_2);
        case 2
            Bth1_=MRsim.Bth1fun2(th_1,th_2);
            Bth2_=MRsim.Bth2fun2(th_1,th_2);
        case 1
            Bth1_=MRsim.Bth1fun1(th_1,th_2);
            Bth2_=MRsim.Bth2fun1(th_1,th_2);
    end    
end
end