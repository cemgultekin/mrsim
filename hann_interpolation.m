function xi = hann_interpolation(t, Tmax, x)
    Nb = size(x,1)-1;
    if length(t)>1
        t=reshape(t,[],1);
    end
    s=t/Tmax*Nb;
    x1 = x(min(max(floor(s),0),Nb)+1,:);
    x2 = x(min(max(ceil (s),0),Nb)+1,:);
    s=pi*mod(s, 1)/2;
    xi = x1 .* cos(s).^2 + ...
          x2 .* sin(s).^2;
end