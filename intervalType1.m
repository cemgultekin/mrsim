function [M,err,...
    Mth1,Mth2,MTRF,...
    Mth1th1,Mth1th2,Mth1TRF,Mth2th2,Mth2TRF,MTRFTRF]=...
    intervalType1(th1,th2,th_1,th_2,TRF,m0s,T1,T2f,R,T2s,TR,degree,errmes)
%th1=-0.1;th2=0.2;TRF=1e-3;m0s=1e-1;T1=1;T2f=1;R=30;T2s=1e-5;TR=4.5e-3;
n=18;
A=MRsim.M1fun(th1,th2,TRF,m0s,T1,T2f,R,T2s,TR); % (n,n,3)
switch degree
    case 6
        B=MRsim.Bfun6(th_1,th_2);
    case 5
        B=MRsim.Bfun5(th_1,th_2);
    case 4
        B=MRsim.Bfun4(th_1,th_2);
    case 3
        B=MRsim.Bfun3(th_1,th_2);
    case 2
        B=MRsim.Bfun2(th_1,th_2);
    case 1
        B=MRsim.Bfun1(th_1,th_2);
end
B=reshape(B,1,1,1,[]);
ii=[0,cumsum(3.^(1:degree))];
A=reshape(A,1,n,n,1,[]);
X=cell(degree+1,1);
X{1}=eye(n);
X{2}=reshape(A,n,n,1,[]);
for k=2:degree
    X{k+1}=reshape(sum(bsxfun(@times, X{k},A),2),n,n,1,[]);
end
M=X{1};

for k=1:degree
    XBK=bsxfun(@times,X{k+1},B(ii(k)+1:ii(k+1)));
    M=M+(-1)^k*sum(XBK,4);
end
%disp(num2str(rcond(M)));
M=inv(M);
if errmes
    A2inf=0;
    ths=linspace(th_1,th_2,10);
    for k=1:length(ths)
        AA=A(:,:,1)+A(:,:,2)*cos(ths(k))+A(:,:,3)*cos(2*ths(k));
        A2inf=max(A2inf, max(eig(AA'*AA)));
    end
    A2inf=sqrt(A2inf);
    T=th_2-th_1;
    K=T*A2inf;
    if K*exp(K)>1
        err=inf;
    else
        err=1/(1-K*exp(K))*K^(degree+1)/factorial(degree+1);
    end
else
   err=0; 
end
if nargout>2
    Xth1=cell(degree,1);
    Xth2=cell(degree,1);
    XTRF=cell(degree,1);
    Ath1=MRsim.M1th1fun(th1,th2,TRF,m0s,T1,T2f,R,T2s,TR); % (n,n,3)
    Ath2=MRsim.M1th2fun(th1,th2,TRF,m0s,T1,T2f,R,T2s,TR); % (n,n,3)
    ATRF=MRsim.M1TRFfun(th1,th2,TRF,m0s,T1,T2f,R,T2s,TR); % (n,n,3)
    if th2-th1>th_2-th_1
        m=round((th2-th1)/(th_2-th_1));
        k=round(m*(th_1-th1)/(th2-th1));
    else
        m=1;
        k=0;
    end
    %gradtransfer=[1-k/m,k/m;1-(k+1)/m,(k+1)/m];
    switch degree
        case 6
            Bth1_=MRsim.Bth1fun6(th_1,th_2);
            Bth2_=MRsim.Bth2fun6(th_1,th_2);
        case 5
            Bth1_=MRsim.Bth1fun5(th_1,th_2);
            Bth2_=MRsim.Bth2fun5(th_1,th_2);
        case 4
            Bth1_=MRsim.Bth1fun4(th_1,th_2);
            Bth2_=MRsim.Bth2fun4(th_1,th_2);
        case 3
            Bth1_=MRsim.Bth1fun3(th_1,th_2);
            Bth2_=MRsim.Bth2fun3(th_1,th_2);
        case 2
            Bth1_=MRsim.Bth1fun2(th_1,th_2);
            Bth2_=MRsim.Bth2fun2(th_1,th_2);
        case 1
            Bth1_=MRsim.Bth1fun1(th_1,th_2);
            Bth2_=MRsim.Bth2fun1(th_1,th_2);
    end
    Bth1=(1-k/m)*Bth1_+(1-(k+1)/m)*Bth2_;
    Bth2=k/m*Bth1_+(k+1)/m*Bth2_;
    
    Bth1=reshape(Bth1,1,1,1,[]);
    Bth2=reshape(Bth2,1,1,1,[]);
    Ath1=reshape(Ath1,1,n,n,1,[]);
    Ath2=reshape(Ath2,1,n,n,1,[]);
    ATRF=reshape(ATRF,1,n,n,1,[]);
    k=1;
    Xth1{k}=reshape(sum(bsxfun(@times, X{k},Ath1),2),n,n,1,[]);
    Xth2{k}=reshape(sum(bsxfun(@times, X{k},Ath2),2),n,n,1,[]);
    XTRF{k}=reshape(sum(bsxfun(@times, X{k},ATRF),2),n,n,1,[]);
    for k=2:degree
        Xth1{k}=reshape(sum(bsxfun(@times, X{k},Ath1),2),n,n,1,[])+...
            reshape(sum(bsxfun(@times, Xth1{k-1},A),2),n,n,1,[]);
        Xth2{k}=reshape(sum(bsxfun(@times, X{k},Ath2),2),n,n,1,[])+...
            reshape(sum(bsxfun(@times, Xth2{k-1},A),2),n,n,1,[]);
        XTRF{k}=reshape(sum(bsxfun(@times, X{k},ATRF),2),n,n,1,[])+...
            reshape(sum(bsxfun(@times, XTRF{k-1},A),2),n,n,1,[]);
    end
    Mth1=M.*0;
    Mth2=Mth1;
    MTRF=Mth2;
    for k=1:degree
        XBK=bsxfun(@times,Xth1{k},B(ii(k)+1:ii(k+1)));
        Mth1=Mth1+(-1)^k*sum(XBK,4);
        XBK=bsxfun(@times,X{k+1},Bth1(ii(k)+1:ii(k+1)));
        Mth1=Mth1+(-1)^k*sum(XBK,4);
        
        XBK=bsxfun(@times,Xth2{k},B(ii(k)+1:ii(k+1)));
        Mth2=Mth2+(-1)^k*sum(XBK,4);
        XBK=bsxfun(@times,X{k+1},Bth2(ii(k)+1:ii(k+1)));
        Mth2=Mth2+(-1)^k*sum(XBK,4);
        
        XBK=bsxfun(@times,XTRF{k},B(ii(k)+1:ii(k+1)));
        MTRF=MTRF+(-1)^k*sum(XBK,4);
    end
    if nargout==5
        Mth1=-M*Mth1*M;
        Mth2=-M*Mth2*M;
        MTRF=-M*MTRF*M;
        return;
    end
end
if nargout>5
    Xth1th1=cell(degree,1);
    Xth1th2=cell(degree,1);
    Xth1TRF=cell(degree,1);
    Xth2th2=cell(degree,1);
    Xth2TRF=cell(degree,1);
    XTRFTRF=cell(degree,1);
      
    Ath1th1=MRsim.M1th1th1fun(th1,th2,TRF,m0s,T1,T2f,R,T2s,TR); % (n,n,3)
    Ath1th2=MRsim.M1th1th2fun(th1,th2,TRF,m0s,T1,T2f,R,T2s,TR); % (n,n,3)
    Ath1TRF=MRsim.M1th1TRFfun(th1,th2,TRF,m0s,T1,T2f,R,T2s,TR); % (n,n,3)
    
    Ath2th2=MRsim.M1th2th2fun(th1,th2,TRF,m0s,T1,T2f,R,T2s,TR); % (n,n,3)
    Ath2TRF=MRsim.M1th2TRFfun(th1,th2,TRF,m0s,T1,T2f,R,T2s,TR); % (n,n,3)
    
    ATRFTRF=MRsim.M1TRFTRFfun(th1,th2,TRF,m0s,T1,T2f,R,T2s,TR); % (n,n,3)
    
    if th2-th1>th_2-th_1
        m=round((th2-th1)/(th_2-th_1));
        k=round(m*(th_1-th1)/(th2-th1));
    else
        m=1;
        k=0;
    end
    %gradtransfer=[1-k/m,k/m;1-(k+1)/m,(k+1)/m];
    switch degree
        case 6
            Bth1th1_=MRsim.Bth1th1fun6(th_1,th_2);
            Bth1th2_=MRsim.Bth1th2fun6(th_1,th_2);
            Bth2th2_=MRsim.Bth2th2fun6(th_1,th_2);
        case 5
            Bth1th1_=MRsim.Bth1th1fun5(th_1,th_2);
            Bth1th2_=MRsim.Bth1th2fun5(th_1,th_2);
            Bth2th2_=MRsim.Bth2th2fun5(th_1,th_2);
        case 4
            Bth1th1_=MRsim.Bth1th1fun4(th_1,th_2);
            Bth1th2_=MRsim.Bth1th2fun4(th_1,th_2);
            Bth2th2_=MRsim.Bth2th2fun4(th_1,th_2);
        case 3
            Bth1th1_=MRsim.Bth1th1fun3(th_1,th_2);
            Bth1th2_=MRsim.Bth1th2fun3(th_1,th_2);
            Bth2th2_=MRsim.Bth2th2fun3(th_1,th_2);
        case 2
            Bth1th1_=MRsim.Bth1th1fun2(th_1,th_2);
            Bth1th2_=MRsim.Bth1th2fun2(th_1,th_2);
            Bth2th2_=MRsim.Bth2th2fun2(th_1,th_2);
        case 1
            Bth1th1_=MRsim.Bth1th1fun1(th_1,th_2);
            Bth1th2_=MRsim.Bth1th2fun1(th_1,th_2);
            Bth2th2_=MRsim.Bth2th2fun1(th_1,th_2);
    end
    Bth1th1_=reshape(Bth1th1_,1,1,[]);
    Bth1th2_=reshape(Bth1th2_,1,1,[]);
    Bth2th2_=reshape(Bth2th2_,1,1,[]);
    BB=cat(1,Bth1th1_,Bth1th2_);
    BB=cat(2,BB,cat(1,Bth1th2_,Bth2th2_));
    BB=reshape(BB,1,2,2,[]);
    G=[(1-k/m), k/m;(1-(k+1)/m),(k+1)/m];
    BB=bsxfun(@times,G.' , BB);
    BB=sum(BB,2);
    BB=reshape(BB,2,2,1,[]);
    BB=bsxfun(@times,BB,reshape(G,1,2,2));
    BB=sum(BB,2);
    
    Bth1th1=BB(1,1,1,:);
    Bth1th2=BB(1,1,2,:);
    Bth2th2=BB(2,1,2,:);
    
    
    Ath1th1=reshape(Ath1th1,1,n,n,1,[]);
    Ath1th2=reshape(Ath1th2,1,n,n,1,[]);
    Ath1TRF=reshape(Ath1TRF,1,n,n,1,[]);
    Ath2th2=reshape(Ath2th2,1,n,n,1,[]);
    Ath2TRF=reshape(Ath2TRF,1,n,n,1,[]);
    ATRFTRF=reshape(ATRFTRF,1,n,n,1,[]);
    k=1;
    Xth1th1{k}=reshape(sum(bsxfun(@times, X{k},Ath1th1),2),n,n,1,[]);
    Xth1th2{k}=reshape(sum(bsxfun(@times, X{k},Ath1th2),2),n,n,1,[]);
    Xth1TRF{k}=reshape(sum(bsxfun(@times, X{k},Ath1TRF),2),n,n,1,[]);
    
    Xth2th2{k}=reshape(sum(bsxfun(@times, X{k},Ath2th2),2),n,n,1,[]);
    Xth2TRF{k}=reshape(sum(bsxfun(@times, X{k},Ath2TRF),2),n,n,1,[]);
    
    XTRFTRF{k}=reshape(sum(bsxfun(@times, X{k},ATRFTRF),2),n,n,1,[]);
    
    
    for k=2:degree
        Xth1th1{k}=reshape(sum(bsxfun(@times, X{k},Ath1th1),2),n,n,1,[])+...
            2*reshape(sum(bsxfun(@times, Xth1{k-1},Ath1),2),n,n,1,[])+...
            reshape(sum(bsxfun(@times, Xth1th1{k-1},A),2),n,n,1,[]);
        
        Xth1th2{k}=reshape(sum(bsxfun(@times, X{k},Ath1th2),2),n,n,1,[])+...
            reshape(sum(bsxfun(@times, Xth1{k-1},Ath2),2),n,n,1,[])+...
            reshape(sum(bsxfun(@times, Xth2{k-1},Ath1),2),n,n,1,[])+...
            reshape(sum(bsxfun(@times, Xth1th2{k-1},A),2),n,n,1,[]);
        
        Xth1TRF{k}=reshape(sum(bsxfun(@times, X{k},Ath1TRF),2),n,n,1,[])+...
            reshape(sum(bsxfun(@times, Xth1{k-1},ATRF),2),n,n,1,[])+...
            reshape(sum(bsxfun(@times, XTRF{k-1},Ath1),2),n,n,1,[])+...
            reshape(sum(bsxfun(@times, Xth1TRF{k-1},A),2),n,n,1,[]);
        
        Xth2th2{k}=reshape(sum(bsxfun(@times, X{k},Ath2th2),2),n,n,1,[])+...
            2*reshape(sum(bsxfun(@times, Xth2{k-1},Ath2),2),n,n,1,[])+...
            reshape(sum(bsxfun(@times, Xth2th2{k-1},A),2),n,n,1,[]);
        Xth2TRF{k}=reshape(sum(bsxfun(@times, X{k},Ath2TRF),2),n,n,1,[])+...
            reshape(sum(bsxfun(@times, Xth2{k-1},ATRF),2),n,n,1,[])+...
            reshape(sum(bsxfun(@times, XTRF{k-1},Ath2),2),n,n,1,[])+...
            reshape(sum(bsxfun(@times, Xth2TRF{k-1},A),2),n,n,1,[]);
        
        XTRFTRF{k}=reshape(sum(bsxfun(@times, X{k},ATRFTRF),2),n,n,1,[])+...
            2*reshape(sum(bsxfun(@times, XTRF{k-1},ATRF),2),n,n,1,[])+...
            reshape(sum(bsxfun(@times, XTRFTRF{k-1},A),2),n,n,1,[]);
    end
    Mth1th1=M.*0;
    Mth1th2=Mth1th1;
    Mth1TRF=Mth1th1;
    
    Mth2th2=Mth1th1;
    Mth2TRF=Mth1th1;
    
    MTRFTRF=Mth1th1;
    for k=1:degree
        XBK=bsxfun(@times,Xth1th1{k},B(ii(k)+1:ii(k+1)));
        Mth1th1=Mth1th1+(-1)^k*sum(XBK,4);
        XBK=2*bsxfun(@times,Xth1{k},Bth1(ii(k)+1:ii(k+1)));
        Mth1th1=Mth1th1+(-1)^k*sum(XBK,4);
        XBK=bsxfun(@times,X{k+1},Bth1th1(ii(k)+1:ii(k+1)));
        Mth1th1=Mth1th1+(-1)^k*sum(XBK,4);
        
        XBK=bsxfun(@times,Xth1th2{k},B(ii(k)+1:ii(k+1)));
        Mth1th2=Mth1th2+(-1)^k*sum(XBK,4);
        XBK=bsxfun(@times,Xth1{k},Bth2(ii(k)+1:ii(k+1)));
        Mth1th2=Mth1th2+(-1)^k*sum(XBK,4);
        XBK=bsxfun(@times,Xth2{k},Bth1(ii(k)+1:ii(k+1)));
        Mth1th2=Mth1th2+(-1)^k*sum(XBK,4);
        XBK=bsxfun(@times,X{k+1},Bth1th2(ii(k)+1:ii(k+1)));
        Mth1th2=Mth1th2+(-1)^k*sum(XBK,4);
        
        XBK=bsxfun(@times,Xth1TRF{k},B(ii(k)+1:ii(k+1)));
        Mth1TRF=Mth1TRF+(-1)^k*sum(XBK,4);
        XBK=bsxfun(@times,XTRF{k},Bth1(ii(k)+1:ii(k+1)));
        Mth1TRF=Mth1TRF+(-1)^k*sum(XBK,4);
        
        
        
        
        XBK=bsxfun(@times,Xth2TRF{k},B(ii(k)+1:ii(k+1)));
        Mth2TRF=Mth2TRF+(-1)^k*sum(XBK,4);
        XBK=bsxfun(@times,XTRF{k},Bth2(ii(k)+1:ii(k+1)));
        Mth2TRF=Mth2TRF+(-1)^k*sum(XBK,4);
        
        XBK=bsxfun(@times,Xth2th2{k},B(ii(k)+1:ii(k+1)));
        Mth2th2=Mth2th2+(-1)^k*sum(XBK,4);
        XBK=2*bsxfun(@times,Xth2{k},Bth2(ii(k)+1:ii(k+1)));
        Mth2th2=Mth2th2+(-1)^k*sum(XBK,4);
        XBK=bsxfun(@times,X{k+1},Bth2th2(ii(k)+1:ii(k+1)));
        Mth2th2=Mth2th2+(-1)^k*sum(XBK,4);
        
        
        XBK=bsxfun(@times,XTRFTRF{k},B(ii(k)+1:ii(k+1)));
        MTRFTRF=MTRFTRF+(-1)^k*sum(XBK,4);
        
    end
    
    Mth1th1=2*M*Mth1*M*Mth1*M-M*Mth1th1*M;
    Mth1th2=M*Mth2*M*Mth1*M+M*Mth1*M*Mth2*M-M*Mth1th2*M;
    Mth1TRF=M*Mth1*M*MTRF*M+M*MTRF*M*Mth1*M-M*Mth1TRF*M;
   
    Mth2th2=2*M*Mth2*M*Mth2*M-M*Mth2th2*M;
    Mth2TRF=M*Mth2*M*MTRF*M+M*MTRF*M*Mth2*M-M*Mth2TRF*M;
    
    MTRFTRF=2*M*MTRF*M*MTRF*M-M*MTRFTRF*M;
    
    Mth1=-M*Mth1*M;
    Mth2=-M*Mth2*M;
    MTRF=-M*MTRF*M;
end
end
%%
%{
%For Testing Purposes
rng(0);
numimage=20;
figure('Position',[200 200 1500 400]);
for ll=1:numimage
th1=-pi/2*rand();th2=pi/2*rand();TRF=1e-3*rand();m0s=rand();T1=2*rand();T2f=2*rand();R=200*rand();T2s=1e-5*rand();TR=4.5e-3;
y0=randn(18,1);y0(3:3:end)=0;y0(3)=1;
A=MRsim.Mfun(th1,th2,TRF,m0s,T1,T2f,R,T2s,TR);
Afun=@(th) A(:,:,1)+A(:,:,2).*cos(th)+A(:,:,3).*cos(2*th);
[M,Mth1,Mth2,MTRF,err]=MRsim.itervalCompute(th1,th2,th2,TRF,m0s,T1,T2f,R,T2s,TR,1);
I=eye(18);
Mode=zeros(18);
opts = odeset('RelTol',1e-9,'AbsTol',1e-12);
for k=1:18
sol=ode15s(@(th,y) Afun(th)*y,[th1,th2],I(:,k),opts);
Mode(:,k)=sol.y(:,end);
end
subplot(1,4,1);
imagesc(log10(abs(Mode-M)));
title('log10(AbsErr)');
colorbar;
subplot(1,4,2);
imagesc(log10(abs(Mode-M)./abs(M)));
title('log10(RelErr)');
colorbar;
subplot(1,4,3);
imagesc(log10(abs(M)));
xlabel(['Analytic RelErr= ',num2str(err)]);
title('exponentiation');
colorbar;
subplot(1,4,4);
imagesc(log10(abs(Mode)));
title('odesolver');
colorbar;
saveas(gcf,['funmatrix',num2str(ll),'.png']);
end






rng(0);
numimage=20;
for ll=1:numimage
th1=-pi/2*rand();th2=pi/2*rand();TRF=1e-3*rand();m0s=rand();T1=2*rand();T2f=2*rand();R=200*rand();T2s=1e-5*rand();TR=4.5e-3;
y0=randn(18,1);y0(3:3:end)=0;y0(3)=1;
A=MRsim.Mfun(th1,th2,TRF,m0s,T1,T2f,R,T2s,TR);
Afun=@(th) A(:,:,1)+A(:,:,2).*cos(th)+A(:,:,3).*cos(2*th);
opts = odeset('RelTol',1e-9,'AbsTol',1e-12);
sol=ode15s(@(th,y) Afun(th)*y,[th1,th2],y0,opts);
y=zeros(18,100);
th=linspace(th1+1e-1,th2,size(y,2));
for k=1:size(y,2)
M=MRsim.itervalCompute(th1,th2,th(k),TRF,m0s,T1,T2f,R,T2s,TR,0);
y(:,k)=M*y0;
end
clf;
m=(1:18);
m(3:3:end)=[];
for k=1:12
subplot(3,4,k);
plot(th,y(m(k),:),'b');
hold on;
plot(sol.x,sol.y(m(k),:),'r');
if k==1
legend('exponentiation','ode15s')
end
if k==1
xlabel(['[m0s,T1,T2f,R,T2s]'])
end
if k==5
xlabel(num2str([m0s,T1,T2f,R,T2s],2));
end
end
saveas(gcf,['trajectories',num2str(ll),'.png']);
end
%}